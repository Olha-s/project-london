import { FC, ReactNode } from 'react'
import { Col, Layout, Row } from 'antd'
import Slider from '../../components/carousel/Slider'
import ItemSlider, { ItemSliderProps } from '../../components/carousel/ItemSlider'
import img01 from './01.png'
import img1 from './card1.png'
import img2 from './card2.jpg'
import img3 from './card3.jpg'
import img4 from './card4.jpg'
import img02 from './02.svg'
import img03 from './03.svg'
import img04 from './04.svg'
import SampleNextArrow from '../../components/carousel/SampleNextArrow'
import SamplePrevArrow from '../../components/carousel/SamplePrevArrow'
import { useMediaQuery } from 'react-responsive'
import { useContent } from '../../containers/CMS'

const Section2: FC = () => {
  const SliderElement = (props: Partial<ItemSliderProps>) => (
    <ItemSlider className='operations-text-wrapper' {...props} />
  )

  const isDesktop = useMediaQuery({ minWidth: 992 })
  const isMobile = useMediaQuery({ maxWidth: 991 })
  const content = useContent()

  // const items = [
  //   { srcImg: img1,
  //     id: 1,
  //     title: `${content?.website.sections.find(el=> el.section.slug === 'operationsSection2')?.section.title}`,
  //     text: `${content?.website.sections.find(el=> el.section.slug === 'operationsSection2')?.section.description}`,
  //     srcNum: img01 },
  //   ]

  const items = [
    {
      srcImg: img1,
      id: 1,
      title: 'Demand Planning',
      text:
        'Our Planning & Forecasting Team is dedicated to identifying and mapping out new program models to analyze data, interpret trends, and provide insight to forecast demands so we can accommodate seasonal, upcoming, and existing market courses efficiently.',
      srcNum: img01,
    },
    {
      srcImg: img2,
      id: 2,
      title: 'Coast to Coast warehousing and Fulfillment',
      text:
        'London Luxury drop ships for all major retailers and fills direct-to-customer orders around the globe. Our bi-coastal facilities in New Jersey and Los Angeles (JPL) produce over 2 million pick and pack programs per year. By operating on each coast, we can ship quickly and efficiently to service both our retail and individual customers.',
      srcNum: img02,
    },
    {
      srcImg: img3,
      id: 3,
      title: 'Production Stages & Inventory',
      text:
        'We pride ourselves on monitoring inventory during all stages, to be prepared for trends and fluctuating demand. Using our customized systems, we can make dynamic adjustments depending on where orders stand in the production process.',
      srcNum: img03,
    },
    {
      srcImg: img4,
      id: 3,
      title: 'Door-To-Door Inventory Tracking',
      text:
        'From initial shipping to final delivery, we maintain round-the-clock visibility of each product in our system. Our supply chain and logistics teams coordinate all global deliveries, and work closely with freight forwarders and customs agents to ensure seamless, on-time delivery while providing our clients with detailed shipment reports throughout the process.',
      srcNum: img04,
    },
  ]

  const settings = {
    dots: false,
    // fade: true,
    infinite: true,
    speed: 1500,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    autoplaySpeed: 5000,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    adaptiveHeight: true,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    responsive: [
      {
        breakpoint: 992,
        settings: {
          fade: false,
          dots: false,
          infinite: true,
          speed: 1500,
          arrows: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 0,
          swipeToSlide: true,
          autoplaySpeed: 5000,
          adaptiveHeight: true,
          nextArrow: <SampleNextArrow />,
          prevArrow: <SamplePrevArrow />,
          cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
        },
      },
    ],
  }
  return (
    <>
      {isDesktop && (
        <Layout
          style={{
            width: '100%',
            background: '#E5DBD1',
          }}
        >
          <Row wrap>
            <Col span={24}>
              <Slider settings={settings} wrapperClassName='operations-slider'>
                {items.map(SliderElement)}
              </Slider>
            </Col>
          </Row>
        </Layout>
      )}

      {isMobile && (
        <Layout
          style={{
            width: '100%',
          }}
        >
          <Row wrap justify={'space-between'} className='operations-block-mob'>
            <Col span={24}>
              <Slider settings={settings} wrapperClassName='operations-slider-mob'>
                {items.map(SliderElement)}
              </Slider>
            </Col>
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section2 }
