import { FC } from 'react'
import { Layout } from 'antd'
import { Section1 } from './Section1'
import { Section2 } from './Section2'

const Operations: FC = () => (
  <Layout>
    <Section1 />
    <Section2 />
  </Layout>
)

export { Operations }
