import { FC, ReactNode } from 'react'
import { Col, Layout, Row, Typography } from 'antd'
import backgroundSrc from './bg-operations-section1.png'
import backgroundSrcSmall from './bg-operations-section1-mob.png'
import './Section1.less'
import { useMediaQuery } from 'react-responsive'
import { useContent } from '../../containers/CMS'

export type SectionProps = {
  src?: string
  srcSmall?: string
  text?: ReactNode
  heading?: ReactNode
}

const Section1: FC = () => {
  const isDesktop = useMediaQuery({ minWidth: 768 })
  const isMobile = useMediaQuery({ maxWidth: 767 })
  const content = useContent()

  const item: Partial<SectionProps>[] = [
    {
      src: backgroundSrc,
      srcSmall: backgroundSrcSmall,
      heading: `${content?.website.sections.find(el => el.section.slug === 'operationsSection1')?.section.title}`,
      text: `${content?.website.sections.find(el => el.section.slug === 'operationsSection1')?.section.description}`,
    },
  ]

  return (
    <>
      {isDesktop && (
        <Layout
          style={{
            background: `url(${item[0].src}) no-repeat`,
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            // paddingTop: '285px',
            // height: '962px',
            width: '100%',
          }}
          className='layout-wrapper'
        >
          <Row
            wrap
            style={{
              height: '100%',
            }}
          >
            <Col
              style={{ height: '73.08%', display: 'flex', alignItems: 'center' }}
              md={{ span: 20, offset: 2 }}
              lg={{ span: 20, offset: 2 }}
              xl={{ span: 16, offset: 4 }}
              xxl={{ span: 16, offset: 4 }}
              className='operations-section1-heading'
            >
              {item[0].heading}
            </Col>
            <Col
              style={{ maxHeight: '26.92%', height: '100%' }}
              sm={20}
              md={18}
              lg={17}
              xl={17}
              xxl={16}
              className='operations-section1-text'
            >
              {item[0].text}
            </Col>
          </Row>
        </Layout>
      )}

      {isMobile && (
        <Layout>
          <Row
            wrap
            align={'middle'}
            justify={'center'}
            style={{
              backgroundImage: `url(${item[0].srcSmall})`,
              backgroundRepeat: 'no-repeat',
              backgroundSize: 'cover',
              height: '657px',
              width: '100%',
              marginTop: '80px',
            }}
          >
            <Col span={20} className='operations-section1-heading-mob'>
              {item[0].heading}
            </Col>
          </Row>
          <Row>
            <Col span={24} className='operations-section1-text-wrapper-mob'>
              <Typography.Paragraph className='operations-section1-text-mob'>{item[0].text}</Typography.Paragraph>
            </Col>
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section1 }
