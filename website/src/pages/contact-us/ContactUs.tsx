import { FC } from 'react'
import { Layout, Row, Col } from 'antd'
import { ContactForm } from '../../components/contact/ContactForm'
import { useMediaQuery } from 'react-responsive'
import { SectionMap } from '../../components/map/SectionMap'

const ContactUs: FC = () => {
  const isDesktop = useMediaQuery({ minWidth: 769 })
  return isDesktop ? (
    <Layout style={{ marginTop: '80px', paddingTop: '36px', background: '#F3EDE8' }}>
      <Row
        align={'middle'}
        style={{ background: '#F3EDE8', maxHeight: '839px', minHeight: '671px', height: '100%' }}
        justify={'center'}
      >
        <Col md={22} lg={18} xl={14} xxl={10} style={{ paddingRight: '40px', paddingLeft: '40px' }}>
          <ContactForm />
        </Col>
      </Row>
      <SectionMap />
    </Layout>
  ) : (
    <Layout style={{ marginTop: '80px' }}>
      <Row style={{ background: '#F3EDE8', paddingTop: '32px' }} justify={'center'} align={'middle'}>
        <Col span={22}>
          <ContactForm />
        </Col>
      </Row>
      <SectionMap />
    </Layout>
  )
}

export { ContactUs }
