import { FC, ReactNode } from 'react'
import { Button, Col, Layout, Row, Typography } from 'antd'
import backgroundSrc from './bg-company-section1.jpg'
import backgroundSrcSmall from './bg-company-section1-mob.png'
import './Section1.less'
import { useMediaQuery } from 'react-responsive'
import { useContent } from '../../containers/CMS'

type SectionProps = { heading: ReactNode; paragraph: ReactNode; text: ReactNode }
const Section1: FC = () => {
  const content = useContent()
  const isDesktop = useMediaQuery({ minWidth: 992 })
  const isMobile = useMediaQuery({ maxWidth: 991 })
  const item = [
    {
      src: backgroundSrc,
      srcSmall: backgroundSrcSmall,
      heading: `${content?.website.sections.find(el => el.section.slug === 'companySection1')?.section.title}`,
      paragraph: `${content?.website.sections.find(el => el.section.slug === 'companySection1')?.section.subTitle}`,
      text: `${content?.website.sections.find(el => el.section.slug === 'companySection1')?.section.description}`,
    },
  ];

  return (
    <>
      {isDesktop && (
        <Layout
          style={{
            background: `url(${item[0].src}) no-repeat`,
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            width: '100%',
          }}
          className='layout-wrapper'
        >
          <Row justify={'space-between'} style={{flexDirection: 'column', height: '100%'}}>
            <Col />
            <Col>
              <Row justify={'center'} align={'middle'} gutter={[0, 40]}>
                <Col
                  // sm={{ span: 18}}
                  lg={{ span: 20}}
                  className='company-section1-heading'
                >
                  {item[0].heading}
                </Col>
                <Col
                  lg={{ span: 18}}
                  xl={{ span: 14}}
                  xxl={{ span: 12}}
                  className='company-section1-paragraph'
                >
                  {item[0].paragraph}
                </Col>
              </Row>
            </Col>
            <Col
              className='company-section1-text'
            >
              {item[0].text}
            </Col>
          </Row>
        </Layout>
      )}
      {isMobile && (
        <Layout>
          <Row
            wrap
            align={'middle'}
            justify={'center'}
            style={{
              backgroundImage: `url(${item[0].srcSmall})`,
              backgroundRepeat: 'no-repeat',
              backgroundSize: 'cover',
              height: '657px',
              width: '100%',
              marginTop: '80px',
              paddingBottom: '130px',
              paddingTop: '110px',
            }}
          >
            <Col span={22} className='company-section1-heading-mob'>
              {item[0].heading}
            </Col>
            <Col span={20} className='company-section1-paragraph-mob'>
              {item[0].paragraph}
            </Col>
          </Row>
          <Row>
            <Col span={24} className='company-section1-text-wrapper-mob'>
              <Typography.Paragraph className='company-section1-text-mob'>{item[0].text}</Typography.Paragraph>
            </Col>
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section1 }
