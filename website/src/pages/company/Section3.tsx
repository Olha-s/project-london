import { FC, ReactNode, useEffect, useState, useRef, useLayoutEffect } from 'react'
import { Button, Col, Dropdown, Layout, Menu, Row } from 'antd'
import { Carousel } from 'antd'
import './Section3.less'
import img1 from '../../components/carousel/images/01.svg'
import img2 from '../../components/carousel/images/02.svg'
import img3 from '../../components/carousel/images/03.svg'
import img4 from '../../components/carousel/images/04.svg'
import { ReactComponent as BtnArrow } from './btn-arrow.svg'
import SampleNextArrow from '../../components/carousel/SampleNextArrow'
import SamplePrevArrow from '../../components/carousel/SamplePrevArrow'
import { useMediaQuery } from 'react-responsive'

const Section3: FC = () => {
  const isDesktop = useMediaQuery({ minWidth: 1201 })
  const isMobile = useMediaQuery({ maxWidth: 1200 })
  const [isShowID, setIsShowID] = useState(null)
  const [isShow, setIsShow] = useState(false)
  const slidesData = [
    {
      num: img1,
      title: 'Global Manufacturer, Design Development and Sourcing Power House',
      id: 1,
      paragraph1:
        'With 278 employees in 12 countries, London Luxury is a leader in the manufacturing, design, development, and production of consumer health and wellness products.',
      paragraph2:
        'From our corporate headquarters in New York, to our offices around the world, we have built a strong and agile team that allows us to be first to market with new and innovative technology.',
    },
    {
      num: img2,
      title: 'Full Service Omni Channel Marketing & Advertising Experts - In House and Under One Roof',
      id: 2,
      paragraph1:
        'London Luxury handles full service 360 marketing, advertising & sales in-house. We do this to ensure that our dedication to quality and success extends beyond production and design. We view marketing as an art form, and work tirelessly to engage with our target audiences in new and innovative ways that are high impact and brand right.',
      paragraph2: ' ',
    },
    {
      num: img3,
      title: 'In Depth Knowledge of Our Community',
      id: 3,
      paragraph1:
        'We develop collections with a keen knowledge of what our community of buyer’s desire, and a deep understanding of what consumers shop for. The strong relationships we share with our brand and retail partners, allow us to work closely and strategically to create unique collaborations and build meaningful programs that truly resonate with people everywhere they see us.',
      paragraph2: ' ',
    },
    {
      num: img4,
      title: 'Infrastructure Engineered For Maximum Efficiency',
      id: 4,
      paragraph1:
        'Our infrastructure has been engineered for maximum efficiency. Robust systems identify optimal shipping configurations, while our demand planning team works with cutting edge technology & human ingenuity to forecast our customers’ needs months in advance, giving us the ability to accurately, and immaculately  service every one of our large-scale business partner operations and supply chain.',
      paragraph2: ' ',
    },
  ]

  const [nav1, setNav1] = useState(null)
  const [nav2, setNav2] = useState(null)
  const slider1 = useRef(null)
  const slider2 = useRef(null)

  useEffect(() => {
    setNav1(slider1.current)
    setNav2(slider2.current)
  }, [])

  const settingsMain = {
    fade: true,
    dots: false,
    infinite: true,
    speed: 2000,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    autoplaySpeed: 2000,
    adaptiveHeight: false,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  }

  const settingsThumbs = {
    dots: false,
    infinite: true,
    speed: 2000,
    arrows: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: false,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    autoplaySpeed: 2000,
  }

  const onClick = (id: number | null) => {
    setIsShow(!isShow)
    setIsShowID(id)
  }
  // const myRef = useRef(null)
  //
  // useEffect(() => {

  //   if(window.location.hash === "#info"){
  //     window.scrollTo(0, myRef.current.scrollHeight * 2.8)}
  //  // console.log(myRef.current.scrollHeight)
  //       // console.log(myRef)
  // }, [])

  return (
    <>
      {isDesktop && (
        <Layout style={{ width: '100%' }} id={'info'}>
          <Row wrap style={{ height: '100%', background: ' #F9F4F0' }}>
            <Col span={16}>
              <Carousel asNavFor={nav2} ref={slider1} {...settingsMain} className='slider-company'>
                {slidesData.map(slide => (
                  <div className='item-wrapper' key={slide.id}>
                    <div className='img-number'>
                      <img src={slide.num} alt='photo' />
                    </div>
                    <div className='text-wrapper'>
                      <div className='item-title'>{slide.title}</div>
                      <div className='item-paragraph1'>{slide.paragraph1}</div>
                      <div className='item-paragraph2'>{slide.paragraph2}</div>
                    </div>
                  </div>
                ))}
              </Carousel>
            </Col>
            <Col span={8} className='wrapper-slider'>
              <Carousel {...settingsThumbs} asNavFor={nav1} ref={slider2} className='slider2'>
                {slidesData.map(slide => (
                  <div className='wrapper' key={slide.id}>
                    <div className='img-number'>
                      <img src={slide.num} alt='photo' />
                    </div>
                    <div className='text'>
                      <div className='title'>{slide.title}</div>
                    </div>
                  </div>
                ))}
              </Carousel>
            </Col>
          </Row>
        </Layout>
      )}
      {isMobile && (
        <Layout style={{ width: '100%', background: '#F9F4F0' }}>
          <Row wrap style={{ height: '100%' }} justify={'center'} align={'middle'}>
            {slidesData.map(el => (
              <Col
                key={el.id}
                span={24}
                style={{
                  paddingTop: '32px',
                  paddingBottom: '32px',
                  borderBottom: '1px solid #D3C6BC',
                  minHeight: '149px',
                }}
              >
                <Col span={20} offset={2} style={{ display: 'flex' }}>
                  <Col span={21} style={{ fontSize: '24px', lineHeight: '28px', color: '#091513', fontFamily: 'Reckless TRIAL' }}>
                    {el.title}
                  </Col>
                  <Col span={3} style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <Button
                      onClick={() => onClick(el.id)}
                      className={isShow && isShowID === el.id ? 'button-active' : 'button-inactive'}
                      style={{
                        width: '33px',
                        height: '33px',
                        padding: '0',
                        border: '1px solid #0D1B1C',
                        borderRadius: '100%',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    >
                      <BtnArrow width={'12px'} height={'14px'} className='btn-arrow' />
                    </Button>
                  </Col>
                </Col>
                {isShow && isShowID === el.id && (
                  <>
                    <Col
                      span={20}
                      offset={2}
                      style={{ fontSize: '20px', lineHeight: '26px', color: '#364243', marginTop: '40px', fontFamily: 'TTCommons' }}
                    >
                      {el.paragraph1}
                    </Col>
                    {el.paragraph2 && (
                      <Col
                        span={20}
                        offset={2}
                        style={{ fontSize: '20px', lineHeight: '26px', color: '#364243', marginTop: '32px', fontFamily: 'TTCommons' }}
                      >
                        {el.paragraph2}
                      </Col>
                    )}
                  </>
                )}
              </Col>
            ))}
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section3 }
