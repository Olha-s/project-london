import { FC, ReactNode } from 'react'
import { Button, Col, Layout, Row } from 'antd'
import './Section5.less'
import img from './card-section5.png'
import imgSmall from './card-section5-mob.png'
import { useMediaQuery } from 'react-responsive'
import { useContent } from '../../containers/CMS'

const Section5: FC = () => {
  const content = useContent()

  const item = [
    {
      img: img,
      imgSmall: imgSmall,
      name: `${content?.website.sections.find(el => el.section.slug === 'companySection5')?.section.subTitle}`,
      title: `${content?.website.sections.find(el => el.section.slug === 'companySection5')?.section.title}`,
      paragraph: `${content?.website.sections.find(el => el.section.slug === 'companySection5')?.section.description}`,
    },
  ]
  const isDesktop = useMediaQuery({ minWidth: 992 })
  const isMobile = useMediaQuery({ maxWidth: 991 })

  return (
    <>
      {isDesktop && (
        <Layout
          style={{
            background: '#F9F4F0',
            width: '100%',
          }}
          className='layout-wrapper-section5'
        >
          <Row wrap justify={'space-between'}>
            <Col sm={24} md={24} lg={24} xl={12} xxl={12} className='section5-text'>
              <Col span={24} className='name'>
                {item[0].name}
              </Col>
              <Col span={24} className='title'>
                {item[0].title}
              </Col>
              <Col span={24} className='paragraph section5-paragraph'>
                {item[0].paragraph}
              </Col>
              {/*    <span className='btn-wrapper'>*/}
              {/*  <Button  ghost style={{ border: 'none', height: 'auto', padding: '0' , color: '#98877b', boxShadow: 'none'}}>*/}
              {/*    <Btn className='btn-active'  />*/}
              {/*  </Button>*/}
              {/*</span>*/}
            </Col>
            <Col sm={24} md={24} lg={24} xl={12} xxl={12} className='section5-img'>
              <div className='img-wrapper'>
                <img src={item[0].img} alt='photo' style={{ width: '100%' }} />
              </div>
            </Col>
          </Row>
        </Layout>
      )}

      {isMobile && (
        <Layout
          style={{
            background: '#F9F4F0',
            width: '100%',
            paddingTop: '72px',
            paddingBottom: '33px',
          }}
        >
          <Row wrap justify={'center'} align={'middle'}>
            <Col span={21}>
              <Col span={24} className='section5-img-mob'>
                <img src={item[0].imgSmall} alt='photo' style={{ width: '100%' }} />
              </Col>
              <Col span={24} className='name-mob'>
                {item[0].name}
              </Col>
              <Col className='title-mob' span={24}>
                {item[0].title}
              </Col>

              <Col className='paragraph-mob'>{item[0].paragraph}</Col>
              {/*    <span className='btn-wrapper-mob'>*/}
              {/*  <Button ghost style={{ border: 'none', height: 'auto', padding: '0', color: '#98877b'}}>*/}
              {/*    <Btn className='btn-active-mob' width='248.77px' height='49px' />*/}
              {/*  </Button>*/}
              {/*</span>*/}
            </Col>
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section5 }
