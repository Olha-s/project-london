import { FC, ReactNode } from 'react'
import { Col, Layout, Row } from 'antd'
import './Section6.less'
import backgroundSrc from './bg-company-section6.png'
import backgroundSrcSmall from './bg-company-section6-mob.png'
import { Logo } from '../../components/logo/Logo'
import { useMediaQuery } from 'react-responsive'
import { useContent } from '../../containers/CMS'
import { Link } from 'react-router-dom'

const Section6: FC = () => {
  const isDesktop = useMediaQuery({ minWidth: 577 })
  const isMobile = useMediaQuery({ maxWidth: 576 })
  const content = useContent()
  const item = [
    {
      bg: backgroundSrc,
      bgSmall: backgroundSrcSmall,
      title: `${content?.website.sections.find(el => el.section.slug === 'companySection6')?.section.title}`,
      subtitle: `${content?.website.sections.find(el => el.section.slug === 'companySection6')?.section.subTitle}`,
    },
  ]
  return (
    <>
      {isDesktop && (
        <Layout
          style={{
            background: `url(${item[0].bg}) no-repeat`,
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            width: '100%',
            paddingTop: '172.19px',
            paddingBottom: '113px',
            borderTop: '3px solid #E3D2C3',
            borderBottom: '3px solid #E3D2C3',
          }}
        >
          <Row wrap justify={'center'}>
            <Col span={24} style={{ marginBottom: '0' }} className='section6-title'>
              {item[0].title}
            </Col>
            <Col span={24} className='section6-title'>
              {item[0].subtitle}
            </Col>
            <Col span={24} className='section6-logo'>
              <Link to='/'>
                <Logo width={'124.52px'} height={'auto'} />
              </Link>
            </Col>
          </Row>
        </Layout>
      )}
      {isMobile && (
        <Layout
          style={{
            backgroundImage: `url(${item[0].bgSmall})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            height: '606px',
            width: '100%',
            display: 'flex',
            justifyContent: 'center',
          }}
        >
          <Row wrap justify={'center'} align={'middle'}>
            <Col span={16} className='section6-title-mob'>
              {item[0].title}
            </Col>
            <Col span={16} className='section6-title-mob'>
              {item[0].subtitle}
            </Col>
            <Col span={20} className='section6-logo mob'>
              <Link to='/'>
                <Logo width={'124.52px'} height={'auto'} />
              </Link>
            </Col>
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section6 }
