import { FC, ReactNode } from 'react'
import { Col, Layout, Row } from 'antd'
import backgroundSrc from './bg-company-section4.png'
import backgroundSrcSmall from './bg-company-section4-mob.png'
import './Section4.less'
import { useMediaQuery } from 'react-responsive'
import { useContent } from '../../containers/CMS'

type SectionProps = { heading: ReactNode }
const Section4: FC = () => {
  const isDesktop = useMediaQuery({ minWidth: 577 })
  const isMobile = useMediaQuery({ maxWidth: 576 })

  const content = useContent()
  const item = [
    {
      src: backgroundSrc,
      srcSmall: backgroundSrcSmall,
      heading: `${content?.website.sections.find(el => el.section.slug === 'companySection4')?.section.title}`,
    },
  ]

  return (
    <>
      {isDesktop && (
        <Layout
          style={{
            backgroundImage: `url(${item[0].src})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            width: '100%',
            paddingTop: '160.5px',
            paddingBottom: '160.5px',
          }}
        >
          <Row
            wrap
            justify={'center'}
            style={{
              height: '100%',
            }}
          >
            <Col span={16} className='section4-block'>
              <Col span={20} offset={2} className='section4-heading'>
                {item[0].heading}
              </Col>
            </Col>
          </Row>
        </Layout>
      )}
      {isMobile && (
        <Layout
          style={{
            background: `url(${item[0].srcSmall})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            height: '606px',
            width: '100%',
            alignContent: 'center',
          }}
        >
          <Row
            wrap
            justify={'center'}
            align={'middle'}
            style={{
              height: '100%',
            }}
          >
            <Col span={20} className='section4-block-mob'>
              <Col span={24} className='section4-heading-mob'>
                {item[0].heading}
              </Col>
            </Col>
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section4 }
