import { FC, ReactNode } from 'react'
import { Col, Layout, Row } from 'antd'
import './Section2.less'
import img1 from './card1-section2.png'
import img2 from './card2-section2.png'
import { useMediaQuery } from 'react-responsive'
import Slider from '../../components/carousel/Slider'
import SampleNextArrow from '../../components/carousel/SampleNextArrow'
import SamplePrevArrow from '../../components/carousel/SamplePrevArrow'
import ItemSlider, { ItemSliderProps } from '../../components/carousel/ItemSlider'

const Section2: FC = () => {
  const isDesktop = useMediaQuery({ minWidth: 992 })
  const isMobile = useMediaQuery({ maxWidth: 991 })
  const SliderElement = (props: Partial<ItemSliderProps>) => (
    <ItemSlider className='company-section2-mob-text-wrapper' {...props} />
  )
  const items = [
    {
      src: img1,
      id: 1,
      paragraph1:
        'A good work-life balance is a key part of our ethos. We are champions of living life to the fullest and creating the most comfortable, beneficial, useful, healthy and innovative feel-good products for people everywhere.',
    },
    {
      src: img2,
      id: 2,
      paragraph1:
        'Many of us take time to “connect” with our inner selves to find real peace. We bounce around from Soul Cycle to at home Peleton workouts, basketball tournaments, and tennis matches. We make it a point, to look after our mind, body & spirit, and cherish with great appreciation, all blessings from above, every single day.',
      paragraph2:
        'We care deeply about the food we eat — and we love “good” food. Whole Foods & Trader Joe’s, are our stomping grounds. We don’t only love what we create, we live it luxuriously.',
    },
  ]

  const settings = {
    dots: false,
    fade: true,
    infinite: true,
    speed: 1500,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    // autoplay: true,
    autoplaySpeed: 5000,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    adaptiveHeight: true,
  }

  return (
    <>
      {isDesktop && (
        <Layout
          style={{
            width: '100%',
          }}
        >
          <Row wrap justify={'space-between'} className='section2 block'>
            <Col sm={24} md={24} lg={24} xl={12} xxl={12} className='section2-img1'>
              <img src={items[0].src} alt='photo' style={{ width: '100%' }} />
            </Col>
            <Col sm={24} md={24} lg={24} xl={12} xxl={12} className='section2-paragraph'>
              <p style={{marginBottom: 0}}>{items[0].paragraph1}</p>

            </Col>
          </Row>
          <Row wrap justify={'space-between'} className='section2 block2'>
            <Col
              sm={{ order: 2, span: 24 }}
              md={{ order: 2, span: 24 }}
              lg={{ order: 2, span: 24 }}
              xl={{ order: 1, span: 12 }}
              xxl={{ order: 1, span: 12 }}
              className='section2-paragraph2'
            >
              <Col span={24} className='paragraph1'>
                <p>{items[1].paragraph1}</p>
                <p>{items[1].paragraph2}</p>
              </Col>
            </Col>
            <Col
              sm={{ order: 1, span: 24 }}
              md={{ order: 1, span: 24 }}
              lg={{ order: 1, span: 24 }}
              xl={{ order: 2, span: 12 }}
              xxl={{ order: 2, span: 12 }}
              className='section2-img2'
            >
              <img src={items[1].src} alt='photo' style={{ width: '100%' }} />
            </Col>
          </Row>
        </Layout>
      )}
      {isMobile && (
        <Layout
          style={{
            width: '100%',
          }}
        >
          <Row wrap justify={'space-between'} className='section2-block-mob'>
            <Col span={24}>
              <Slider settings={settings} wrapperClassName='company-section2-slider-mob'>
                {items.map(SliderElement)}
              </Slider>
            </Col>
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section2 }
