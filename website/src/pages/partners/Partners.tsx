import { FC, useEffect } from 'react'
import { Layout } from 'antd'
import { Section1 } from './Section1'
import { Section2 } from './Section2'
import { trademarkReduce } from '../../containers'

const Partners: FC = () => {
  useEffect(() => {
    trademarkReduce(['partners-paragraph1'])
  }, [])
  return (
    <Layout>
      <Section1 />
      <Section2 />
    </Layout>
  )
}
export { Partners }
