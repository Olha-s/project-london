import { FC, ReactNode, useEffect, useRef, useState } from 'react'
import { Carousel, Col, Layout, Row } from 'antd'
import img01Act from './image01-act.png'
import img01 from './image01.png'
import imgReebok from './image02-act.png'
import img02Act from './Reebok_delta_logo.svg'
import img02 from './image02.png'
import img03Act from './image03-act.png'
import img03 from './image03.png'
import img04Act from './image04-act.png'
import img04 from './image04.png'
import img05Act from './image05-act.png'
import img05 from './image05.png'
import img06Act from './image06-act.png'
import img06 from './image06.png'
import img07Act from './image07-act.png'
import img07 from './image07.png'
import img08 from './brookstone-logo.png'
import slide3 from './slide03.png'
import slide1 from './slide01.jpg'
import slide2 from './slide02.jpg'
import slide4 from './slide04.jpg'
import slide5 from './slide05.jpg'
import slide6 from './slide06.jpg'
import slide7 from './slide07.jpg'
import slide8 from './brookstone-image.png'
import SampleNextArrow from '../../components/carousel/SampleNextArrow'
import SamplePrevArrow from '../../components/carousel/SamplePrevArrow'
import './Section2.less'
import { useMediaQuery } from 'react-responsive'
import Slider from '../../components/carousel/Slider'
import ItemSlider, { ItemSliderProps } from '../../components/carousel/ItemSlider'

const Section2: FC = () => {
  const SliderElement = (props: Partial<ItemSliderProps>) => (
    <ItemSlider className='partners-section2-mob-text-wrapper' {...props} />
  )

  const isDesktop = useMediaQuery({ minWidth: 992 })
  const isMobile = useMediaQuery({ maxWidth: 991 })

  const slidesData1 = [
    {
      srcAct: img02Act,
      src: img02,
      id: 1,
      num: '01',
    },
    {
      srcAct: img03Act,
      src: img03,
      id: 2,
      num: '02',
    },
    {
      srcAct: img01Act,
      src: img01,
      id: 3,
      num: '03',
    },
    {
      srcAct: img07Act,
      src: img07,
      id: 4,
      num: '04',
    },
    {
      srcAct: img05Act,
      src: img05,
      id: 5,
      num: '05',
    },
    {
      srcAct: img06Act,
      src: img06,
      id: 6,
      num: '06',
    },
    {
      srcAct: img04Act,
      src: img04,
      id: 7,
      num: '07',
    },
    {
      srcAct: img08,
      src: img08,
      id: 8,
      num: '08',
    }
  ]
  const slidesData2 = [
    {
      srcAct: imgReebok,
      src: slide2,
      id: 1,
      paragraph1:
        'Reebok is an American-inspired global brand with a deep heritage in fitness. In fact, our mission is to be the best fitness brand in the world. At Reebok, we know that greatness doesn’t come from standing still. We have a long history of pushing boundaries. We’re the brand that helped usher in a fitness movement that forever changed the way we look at spandex and headbands. It’s not the 1980s anymore but today, we continue to be daring in everything we do. We are curious, provocative, witty and unexpected. We are Reebok.',
    },
    {
      srcAct: img03Act,
      src: slide3,
      id: 2,
      paragraph1:
        'For more than 165 years, people have trusted Arm & Hammer® brand for baking and countless personal care uses. Today consumers appreciate that Arm & Hammer products are versatile, environment safe, and economical',
      paragraph2:
        'Our customer’s everyday needs come first. London Luxury’s practical and affordable products are crafted with exceptional quality and attention to detail in mind.',
    },
    {
      srcAct: img01Act,
      src: slide1,
      id: 3,
      paragraph1:
        'In 1963, Ben Sherman® made history with his original Ben Sherman shirt. By pushing the boundaries of ordinary design, creating signature details and using unexpected prints, the Ben Sherman brand has become truly iconic. For more than 5 decades the Ben Sherman shirt and brand have been adopted by almost every youth culture or style movement, from the mods , to 2 tone and ska, to brit pop and beyond.  Today the brand continues to push boundaries on a global scale, offering a full lifestyle range of products that speak to both classic luxury and modern originality.',
    },
    {
      srcAct: img07Act,
      src: slide7,
      id: 4,
      paragraph1: 'London Luxury has been a leader in anti-allergen bedding for more than 10 years.',
      paragraph2:
        'Enter Claritin® anti-allergen bedding: An effective way to protect bedding against pollen, household allergens, dust mites and pet dander. The full line assortment of products includes: Pillows, Pillow Protectors, Mattress Protectors, Sheet Sets, Comforters, Blankets, Mattress Toppers and more.',
    },
    {
      srcAct: img05Act,
      src: slide5,
      id: 5,
      paragraph1:
        'Nanobionic® was founded in 2011 by George and Ermis Psipsikas and has been successful ever since. Nanobionic offers the next generation technology for textiles, foams and leathers, combining science, technology and nature to enhance your life.',
      paragraph2:
        'Using a specialized patented technology, products powered by Nanobionic technology offer a wide range of wellness and performance benefits. In a constant pursuit of innovation, Nanobionic is dedicated towards forward thinking and product development solutions from our accredited scientific research team.',
    },
    {
      srcAct: img06Act,
      src: slide6,
      id: 6,
      paragraph1:
        'Since 1938, Columbia Sportswear® has set the industry standard in performance-based outdoor apparel, sportswear, and accessories.',
      paragraph2:
        'London Luxury is proud to partner with Columbia to introduce a new line of bedding and linen products that integrate the innovative technologies and durability of the Columbia brand into luxury home accessories. The full line of performance-based products includes pillows, bed sheets, mattress pads, and mattress protectors designed to the highest standard for lasting comfort and superior reliability.',
    },
    {
      srcAct: img04Act,
      src: slide4,
      id: 7,
      paragraph1:
        'Sharper Image® offers consumers an engaging way to find the latest home electronics, air purifiers and other lifestyle products. Designed to be the ultimate shopping experience, our website is both state of the art and easy to use, with informative video clips, customer product ratings and reviews, convenient order tracking and access to real-time, toll-free customer support. SharperImage.com continues the tradition of fun, innovation and excitement that Sharper Image originated in 1977.',
    },
    {
      srcAct: img08,
      src: slide8,
      id: 8,
      paragraph1:
        'Brookstone is an iconic American brand, founded over 50 years ago in New Hampshire. Today a highly recognized brand which develops and offers quality and innovative merchandise, Brookstone’s product is distributed through a multi-channel platform of retail, wholesale, catalog and E-commerce. The company has leveraged the success of its breakthrough products in Entertainment, Wellness, Home & Travel products to become a true lifestyle brand. Brookstone’s legacy of Quality, Innovation and Solution-oriented products span across a broad range of categories.',
    },
  ]

  const [nav1, setNav1] = useState(null)
  const [nav2, setNav2] = useState(null)
  const slider1 = useRef(null)
  const slider2 = useRef(null)

  useEffect(() => {
    setNav1(slider1.current)
    setNav2(slider2.current)
  }, [])

  const settingsMain = {
    dots: false,
    infinite: true,
    speed: 3000,
    arrows: true,
    slidesToShow: 6,
    focusOnSelect: true,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    autoplaySpeed: 5000,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 1500,
        settings: {
          dots: false,
          infinite: true,
          speed: 3000,
          arrows: true,
          slidesToShow: 5,
          focusOnSelect: true,
          slidesToScroll: 1,
          initialSlide: 0,
          swipeToSlide: true,
          autoplaySpeed: 5000,
          cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
          nextArrow: <SampleNextArrow />,
          prevArrow: <SamplePrevArrow />,
        },
      },
      {
        breakpoint: 1200,
        settings: {
          dots: false,
          infinite: true,
          speed: 3000,
          arrows: true,
          slidesToShow: 4,
          focusOnSelect: true,
          slidesToScroll: 1,
          initialSlide: 0,
          swipeToSlide: true,
          autoplaySpeed: 5000,
          cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
          nextArrow: <SampleNextArrow />,
          prevArrow: <SamplePrevArrow />,
        },
      },
    ],
  }

  const settingsThumbs = {
    dots: false,
    fade: true,
    centerMode: true,
    infinite: true,
    speed: 3000,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    autoplaySpeed: 5000,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    responsive: [
      {
        breakpoint: 992,
        settings: {
          centerMode: true,
          fade: true,
          dots: false,
          infinite: true,
          speed: 1500,
          arrows: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 2,
          swipeToSlide: true,
          autoplaySpeed: 5000,
          autoplay: false,
          adaptiveHeight: true,
          nextArrow: <SampleNextArrow />,
          prevArrow: <SamplePrevArrow />,
          cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
        },
      },
    ],
  }
  const settingsThumbs2 = {
    fade: true,
    dots: false,
    infinite: true,
    speed: 1500,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 2,
    swipeToSlide: true,
    autoplaySpeed: 5000,
    autoplay: false,
    adaptiveHeight: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
  }

  return (
    <>
      {isDesktop && (
        <Layout style={{ width: '100%', overflowX: 'hidden' }}>
          <Row wrap style={{ background: '#091513', paddingLeft: '80px' }} justify={'center'}>
            <Col span={24} className='partners-slider2-wrapper'>
              <Carousel asNavFor={nav2} ref={slider1} {...settingsMain} className='partners-slider1'>
                {slidesData1.map(slide => (
                  <div className='partners-item-wrapper1' key={slide.id}>
                    <div className='partners-img'>
                      <span className='wrapper-img'>
                        <img
                          src={slide.srcAct}
                          alt='photo'
                          className={slide.id % 2 !== 0 ? 'partners-img-active-filter' : ''}
                        />
                      </span>
                    </div>
                    <div className='partners-img-active'>
                      <span className='wrapper-img'>
                        <img src={slide.srcAct} alt='photo' />
                      </span>
                    </div>
                  </div>
                ))}
              </Carousel>
            </Col>
          </Row>

          <Row wrap style={{ background: '#F3EDE8' }}>
            <Col span={24}>
              <Carousel {...settingsThumbs} asNavFor={nav1} ref={slider2} className='partners-slider2'>
                {slidesData2.map(slide => (
                  <div className='partners-item-wrapper2' key={slide.id}>
                    <div className='partners-text-wrapper'>
                      <div className='partners-logo'>
                        <img src={slide.srcAct} alt='photo' />
                      </div>
                      <div className='partners-paragraph1'>{slide.paragraph1}</div>
                      {slide.paragraph2 && <div className='partners-paragraph1'>{slide.paragraph2}</div>}
                    </div>

                    <div className='partners-img-wrapper'>
                      <img src={slide.src} alt='photo' />
                    </div>
                  </div>
                ))}
              </Carousel>
            </Col>
          </Row>
        </Layout>
      )}

      {isMobile && (
        <Layout
          style={{
            width: '100%',
            overflowX: 'hidden',
            paddingTop: '45px',
            paddingBottom: '40px',
            background: '#F3EDE8',
          }}
        >
          <Row wrap justify={'space-between'} className='partners-block-mob'>
            <Col span={24}>
              <Slider settings={settingsThumbs2} wrapperClassName='partners-section2-slider-mob'>
                {slidesData2.map(SliderElement)}
              </Slider>
            </Col>
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section2 }
