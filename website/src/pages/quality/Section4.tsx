import { FC, ReactNode } from 'react'
import { Col, Layout, Row, Typography } from 'antd'
import './Section4.less'
import img1 from './card1-section4.png'
import img2 from './card2-section4.png'
import img3 from './card3-section4.png'
import img4 from './card4-section4.png'
import img5 from './card5-section4.png'
import img6 from './card6-section4.png'
import greenLogo from './Oeko-Tex_Made_in_Green_Logo.png'
import tencelLogo from './Tencel_logo.png'
import gotsLogo from './GOTS_logo.png'
import bciLogo from './BCI_logo.png'
import rdsLogo from './RDS_logo.png'
import okoLogo from './Oeko-Tex_Standard_100_Logo.png'
import { useMediaQuery } from 'react-responsive'

const Section4: FC = () => {
  const isDesktop = useMediaQuery({ minWidth: 992 })
  const isMobile = useMediaQuery({ maxWidth: 991 })

  const items = [
    {
      img: img1,
      id: 1,
      title: 'Organic',
      imgLogo: gotsLogo,
      paragraph:
        'We are GOTS Certified, the standard in Organic Certification for all textiles. The Global Organic Textile Standard (GOTS) is the worldwide leading textile processing standard for organic fibers, including ecological and social criteria, backed up by independent certification of the entire textile supply chain.',
      paragraphSmall:
        'We are GOTS Certified, the standard in Organic Certification for textiles. The Global Organic Textile Standard (GOTS)',
    },
    {
      img: img2,
      id: 2,
      title: 'Standard 100 by Oeko-Tex',
      imgLogo: okoLogo,
      paragraph:
        "STANDARD 100 by OEKO-TEX® is one of the world's best-known labels for textiles tested for harmful substances. It stands for customer confidence and high product safety.",
      paragraphSmall: "STANDARD 100 by OEKO-TEX® is one of the world's best-known labels for textiles tested",
    },
    {
      img: img3,
      id: 3,
      title: 'Made in Green by Oeko-Tex',
      imgLogo: greenLogo,
      paragraph:
        'MADE IN GREEN by OEKO-TEX® is a traceable product label for all kinds of textiles (e.g. garments, home textiles) and leather products (e.g. garments, finished and semi-finished leathers) including non-textile/-leather components (e.g. accessories). The MADE IN GREEN label verifies that an article has been tested for harmful substances. This is carried out through certification in accordance with STANDARD 100 by OEKO-TEX® or LEATHER STANDARD by OEKO-TEX®. It also guarantees that the textile or leather product has been manufactured using sustainable processes under socially responsible working conditions.',
      paragraphSmall:
        'MADE IN GREEN by OEKO-TEX® is a traceable product label for all kinds of textiles (e.g. garments',
    },
    {
      img: img4,
      id: 4,
      title: 'Lenzing Tencel',
      imgLogo: tencelLogo,
      paragraph:
        "TENCEL™ is Lenzing's flagship brand for textiles. Used for a variety of highly specialized applications, the benefits of TENCEL™ fibers can be discerned instantly: Soft to the skin, smooth to the touch, luxurious in shine and flow - they caress the senses. And they also promote comfort with their exceptional capacity for thermal regulation and moisture absorption.",
      paragraphSmall:
        "TENCEL™ is Lenzing's flagship brand for textiles. Used for a variety of highly specialized applications, the benefits of TENCEL™",
    },
    {
      img: img5,
      id: 5,
      title: 'RDS Down',
      imgLogo: rdsLogo,
      paragraph:
        'The Responsible Down Standard (RDS) aims to ensure that down and feathers come from animals that have not been subjected to any unnecessary harm. We believe that education – through the RDS – is a meaningful way to drive demand for strong animal welfare practices.',
      paragraphSmall:
        'The Responsible Down Standard (RDS) aims to ensure that down and feathers come from animals that have not been',
    },
    {
      img: img6,
      id: 6,
      title: 'Better Cotton Initiative',
      imgLogo: bciLogo,
      paragraph:
        'The Better Cotton Initiative (BCI) is a global not-for-profit organization and the largest cotton sustainability program in the world. BCI exists to make global cotton production better for the people who produce it, better for the environment it grows in and better for the sector’s future.',
      paragraphSmall:
        'The Better Cotton Initiative (BCI) is a global not-for-profit organization and the largest cotton',
    },
  ]

  return (
    <>
      {isDesktop && (
        <Layout
          style={{
            width: '100%',
          }}
        >
          <Row className='quality-section4'>
            {items.map(el => (
              <Col span={12} className='quality-section4-wrapper' key={el.id}>
                <div className='quality-section4-img'>
                  <img
                    src={el.img}
                    alt='photo'
                    style={{ width: '100%' }}
                    className={el.id === 5 ? 'quality-section4-img5' : ''}
                  />
                </div>
                <div className='quality-section4-text'>
                  <div className='quality-section4-text-wrapper'>
                    <Typography.Paragraph className='quality-section4-title'>{el.title}</Typography.Paragraph>
                    <Typography.Paragraph className='quality-section4-paragraph'>{el.paragraph}</Typography.Paragraph>
                  </div>
                  {el.imgLogo && (
                    <div className='quality-section4-text-logo'>
                      <img src={el.imgLogo} alt='photo' style={{ width: '100%' }} />
                    </div>
                  )}
                </div>
              </Col>
            ))}
          </Row>
        </Layout>
      )}
      {isMobile && (
        <Layout
          style={{
            width: '100%',
          }}
        >
          <Row wrap justify={'space-between'} className='quality-section4-mob'>
            {items.map(el => (
              <div className='quality-wrapper-mob' key={el.id}>
                <Col span={12} className='quality-section4-wrapper-mob'>
                  <div className='quality-section4-img-mob'>
                    <img src={el.img} alt='photo' style={{ width: '100%' }} />
                  </div>
                </Col>
                <Col span={12} className='quality-section4-text-wrapper-mob'>
                  <div className='quality-section4-text-mob'>
                    <Typography.Paragraph className='quality-section4-title-mob' style={{ marginBottom: '10px' }}>
                      {el.title}
                    </Typography.Paragraph>
                    <Typography.Paragraph className='quality-section4-paragraph-mob' style={{ marginBottom: '0' }}>
                      {el.paragraph}
                    </Typography.Paragraph>
                    {el.imgLogo && <div className='quality-section4-text-logo-mob'><img src={el.imgLogo} alt='photo' style={{ width: '100%' }} /></div>}
                  </div>
                </Col>
              </div>
            ))}
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section4 }
