import { FC, ReactNode } from 'react'
import { Col, Layout, Row } from 'antd'
import Slider from '../../components/carousel/Slider'
import ItemSlider, { ItemSliderProps } from '../../components/carousel/ItemSlider'
import img01 from './01.png'
import img02 from './02.svg'
import img03 from './03.svg'
import img04 from './04.svg'
import img1 from './card1-section3.png'
import img2 from './card2-section3.jpg'
import img3 from './card3-section3.jpg'
import img4 from './card4-section3.jpg'
import SampleNextArrow from '../../components/carousel/SampleNextArrow'
import SamplePrevArrow from '../../components/carousel/SamplePrevArrow'
import { useMediaQuery } from 'react-responsive'
import { useContent } from '../../containers/CMS'

const Section3: FC = () => {
  const isDesktop = useMediaQuery({ minWidth: 992 })
  const isMobile = useMediaQuery({ maxWidth: 991 })
  const SliderElement = (props: Partial<ItemSliderProps>) => <ItemSlider className='quality-text-wrapper' {...props} />
  const content = useContent()

  // const items = [
  //   {
  //     srcImg: img1,
  //     id: 1,
  //     title: `${content?.website.sections.find(el=> el.section.slug === 'qualitySection3')?.section.title}`,
  //     srcNum: img01,
  //   },
  //   {
  //     srcImg: img1,
  //     id: 2,
  //     title: `${content?.website.sections.find(el=> el.section.slug === 'qualitySection3')?.section.title}`,
  //     srcNum: img01,
  //   },
  // ]
  const items = [
    {
      srcImg: img4,
      id: 1,
      title:
        'Dedicated quality, manufacturing and inspection teams in each factory, overseeing every stage of production.',
      srcNum: img01,
    },
    {
      srcImg: img3,
      id: 2,
      title:
        'All factories are third party rated and continuously inspected to ensure we are not only meeting, but also exceeding international standards for social codes of conduct.',
      srcNum: img02,
    },
    {
      srcImg: img1,
      id: 3,
      title: 'All products are inspected, and again reinspected by weave-quality technicians that are responsible for the upkeep of looms and associated equipment. And implement preventative maintenance solutions – adhering to all ISO 9001 regulations and Good Housekeeping Procedures.',
      srcNum: img03,
    },
    {
      srcImg: img2,
      id: 4,
      title: 'Color technicians approve each and every dye lot against the approved original before cutting. This ensure that every product, whether it’s a bed sheet, bath towel, robe, slipper or lounge wear, will always be the exact same color as it was meant to be. And when purchasing additional items of the same product, there will never be shift in color shade.',
      srcNum: img04,
    },
  ]

  const settings = {
    dots: false,
    infinite: true,
    speed: 1500,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    autoplaySpeed: 5000,
    adaptiveHeight: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    responsive: [
      {
        breakpoint: 992,
        settings: {
          fade: false,
          dots: false,
          infinite: true,
          speed: 1500,
          arrows: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 0,
          swipeToSlide: true,
          autoplaySpeed: 5000,
          adaptiveHeight: true,
          nextArrow: <SampleNextArrow />,
          prevArrow: <SamplePrevArrow />,
          cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
        },
      },
    ],
  }
  return (
    <>
      {isDesktop && (
        <Layout
          style={{
            width: '100%',
          }}
        >
          <Row wrap>
            <Col span={24}>
              <Slider settings={settings} wrapperClassName='quality-slider'>
                {items.map(SliderElement)}
              </Slider>
            </Col>
          </Row>
        </Layout>
      )}

      {isMobile && (
        <Layout
          style={{
            width: '100%',
          }}
        >
          <Row wrap justify={'space-between'} className='quality-block-mob'>
            <Col span={24}>
              <Slider settings={settings} wrapperClassName='quality-slider-mob'>
                {items.map(SliderElement)}
              </Slider>
            </Col>
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section3 }
