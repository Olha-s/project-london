import { FC, ReactNode } from 'react'
import { Col, Layout, Row, Typography } from 'antd'
import './Section5.less'
import { useMediaQuery } from 'react-responsive'
import ItemSlider, { ItemSliderProps } from '../../components/carousel/ItemSlider'
import Slider from '../../components/carousel/Slider'
import SampleNextArrow from '../../components/carousel/SampleNextArrow'
import SamplePrevArrow from '../../components/carousel/SamplePrevArrow'
import { useContent } from '../../containers/CMS'

const Section5: FC = () => {
  const isDesktop = useMediaQuery({ minWidth: 768 })
  const isMobile = useMediaQuery({ maxWidth: 767 })

  const SliderElement = (props: Partial<ItemSliderProps>) => (
    <ItemSlider className='quality-section4-mob-text-wrapper' {...props} />
  )

  const content = useContent()
  const items = {
    title: `${content?.website.sections.find(el => el.section.slug === 'qualitySection5')?.section.title}`,
    text: [
      {
        id: 1,
        paragraph: 'Environmentally-friendly and recycled packaging and practices',
      },
      {
        id: 2,
        paragraph: 'Strict adherence to social compliant protocols and procedures',
      },
      {
        id: 3,
        paragraph: 'Approved quality spec standards for all products',
      },
      {
        id: 4,
        paragraph:
          'Dedicated quality inspection teams in all countries we operate, overseeing every stage of production from farm to store.',
      },
      {
        id: 5,
        paragraph: 'Every product goes through an extensive quality assurance process',
      },
      {
        id: 6,
        paragraph: 'All factories are rated and continuously inspected to International Standards',
      },
      {
        id: 7,
        paragraph: 'Dedicated weaving quality technician managers for all products',
      },
      {
        id: 8,
        paragraph: 'Strict adherence to ISO and ECO standards',
      },
      {
        id: 9,
        paragraph: 'Dye lot color technicians approve each and every dye lot against approved original before cutting',
      },
      {
        id: 10,
        paragraph: 'Pre-production MTL testing on all products',
      },
    ],
  }
  const itemsSlider = items['text']
    .filter(el => el.id % 2 !== 0)
    .map(el => ({
      id: el.id,
      paragraph1: el.paragraph,
      paragraph2: items['text'].filter(i => i.id === el.id + 1)[0].paragraph,
    }))

  const settings = {
    dots: false,
    fade: true,
    infinite: true,
    speed: 1500,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    autoplaySpeed: 5000,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    //adaptiveHeight: true
  }

  return (
    <>
      {isDesktop && (
        <Layout
          style={{
            width: '100%',
            background: '#122222',
          }}
          className='section5-layout-wrapper'
        >
          <Row wrap justify={'center'} className='quality-section4'>
            <Col md={20} lg={19} xl={19} xxl={16}>
              <Typography.Paragraph className='quality-section5-title'>{items['title']}</Typography.Paragraph>
            </Col>
            <Col span={12} className='quality-column1'>
              {items['text'].slice(0, 5).map(el => (
                <div key={el.id} className='quality-section5-text'>
                  <div>
                    <span className='circle' />
                  </div>
                  <Typography.Paragraph className='quality-section5-paragraph'>{el.paragraph}</Typography.Paragraph>
                </div>
              ))}
            </Col>
            <Col span={12} className='quality-column2'>
              {items['text'].slice(5, 10).map(el => (
                <div key={el.id} className='quality-section5-text'>
                  <div>
                    <span className='circle' />
                  </div>
                  <Typography.Paragraph className='quality-section5-paragraph'>{el.paragraph}</Typography.Paragraph>
                </div>
              ))}
            </Col>
          </Row>
        </Layout>
      )}

      {isMobile && (
        <Layout
          style={{
            width: '100%',
          }}
        >
          <Row wrap justify={'center'} align={'middle'} className='quality-section5-block-mob'>
            <Col span={22}>
              <Typography.Paragraph className='quality-section5-title-mob' style={{ marginBottom: '56px' }}>
                {items['title']}
              </Typography.Paragraph>
            </Col>
            <Col span={22}>
              <Slider settings={settings} wrapperClassName='quality-section5-slider-mob'>
                {itemsSlider.map(SliderElement)}
              </Slider>
            </Col>
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section5 }
