import { FC, ReactNode } from 'react'
import { Col, Layout, Row } from 'antd'
import backgroundSrc from './bg-quality-section1.png'
import backgroundSrcSmall from './bg-quality-section1-small.png'
import './Section1.less'
import { useMediaQuery } from 'react-responsive'
import { useContent } from '../../containers/CMS'

export type SectionProps = {
  src: string
  heading: string
  srcSmall?: string
}

const Section1: FC = () => {
  const isDesktop = useMediaQuery({ minWidth: 577 })
  const isMobile = useMediaQuery({ maxWidth: 576 })
  const content = useContent()

  const item: Partial<SectionProps>[] = [
    {
      src: backgroundSrc,
      srcSmall: backgroundSrcSmall,
      heading: `${content?.website.sections.find(el => el.section.slug === 'qualitySection1')?.section.title}`,
    },
  ]

  return (
    <>
      {isDesktop && (
        <Layout
          style={{
            background: `url(${item[0].src}) no-repeat`,
            backgroundSize: 'cover',
            width: '100%',
            backgroundPosition: 'center',
          }}
          className='layout-wrapper'
        >
          <Row
            wrap
            style={{
              height: '100%',
            }}
            align={'middle'}
            justify={'center'}
          >
            <Col md={16} lg={14} xl={12} xxl={12} className='quality-section1-heading'>
              {item[0].heading}
            </Col>
          </Row>
        </Layout>
      )}

      {isMobile && (
        <Layout>
          <Row
            wrap
            align={'middle'}
            justify={'center'}
            style={{
              backgroundImage: `url(${item[0].srcSmall})`,
              backgroundRepeat: 'no-repeat',
              backgroundSize: 'cover',
              height: '665px',
              width: '100%',
            }}
          >
            <Col span={20} className='quality-section1-heading-mob'>
              {item[0].heading}
            </Col>
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section1 }
