import { FC, ReactNode } from 'react'
import { Button, Col, Layout, Row } from 'antd'
import img from './card-section2.png'
import imgSmall from './card-section2-small.png'
import './Section2.less'
import { useMediaQuery } from 'react-responsive'
import { useContent } from '../../containers/CMS'

export type SectionProps = {
  img: string
  title: ReactNode
  paragraph: ReactNode
  imgSmall?: string
}

const Section2: FC = () => {
  const content = useContent()
  const isDesktop = useMediaQuery({ minWidth: 992 })
  const isMobile = useMediaQuery({ maxWidth: 991 })

  const item: Partial<SectionProps>[] = [
    {
      img: img,
      imgSmall: imgSmall,
      title: `${content?.website.sections.find(el => el.section.slug === 'qualitySection2')?.section.title}`,
      paragraph: `${content?.website.sections.find(el => el.section.slug === 'qualitySection2')?.section.description}`,
    },
  ]

  return (
    <>
      {isDesktop && (
        <Layout
          style={{
            background: '#F9F4F0',
            width: '100%',
          }}
          className='section2-wrapper'
        >
          <Row wrap justify={'space-between'}>
            <Col span={12} className='quality-section2-img'>
              <div className='quality-section2-img-wrapper'>
                <img src={item[0].img} alt='photo' style={{ width: '100%' }} />
              </div>
            </Col>
            <Col span={12} className='quality-section2-text'>
              <Col span={24} className='quality-section2-title'>
                {item[0].title}
              </Col>
              <Col span={24} className='quality-section2-paragraph'>
                {item[0].paragraph}
              </Col>
            </Col>
          </Row>
        </Layout>
      )}

      {isMobile && (
        <Layout>
          <Row wrap align={'middle'} justify={'center'}>
            <Col span={24} style={{ marginTop: '-1px' }}>
              <img src={item[0].imgSmall} alt='photo' style={{ width: '100%' }} />
            </Col>
            <Col span={24} className='quality-section2-text-mob'>
              <Col span={22} offset={1} className='quality-section2-title-mob'>
                {item[0].title}
              </Col>
              <Col span={22} offset={1} className='quality-section2-paragraph-mob'>
                {item[0].paragraph}
              </Col>
            </Col>
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section2 }
