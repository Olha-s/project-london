import { FC, useEffect } from 'react'
import { Layout } from 'antd'
import { Section1 } from './Section1'
import { Section2 } from './Section2'
import { Section3 } from './Section3'
import { Section4 } from './Section4'
import { Section5 } from './Section5'
import { trademarkReduce } from '../../containers'

const Quality: FC = () => {
    useEffect(() => {
        trademarkReduce(['quality-section4-paragraph'])
    }, [])
    return (
      <Layout>
          <Section1 />
          <Section2 />
          <Section3 />
          <Section4 />
          <Section5 />
      </Layout>
    )
}
export { Quality }
