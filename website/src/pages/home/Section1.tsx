import { FC, useEffect, useRef, useState } from 'react'
import { Col, Layout, Row, Button, Typography } from 'antd'
import backgroundSrc from './bg-section1.png'
import { ReactComponent as Btn } from './btn-section1.svg'
import './Section1.less'
import { useMediaQuery } from 'react-responsive'
import { useContent } from '../../containers/CMS'
import { NavLink } from 'react-router-dom'
import {isIOS} from 'react-device-detect';

const Section1: FC = () => {
  const isDesktop = useMediaQuery({ minWidth: 992 })
  const {
    website: { sections },
  } = useContent()
  const item = {
    heading: `${sections.find(el => el.section.slug === 'homeSection1')?.section.title}`,
    paragraph: `${sections.find(el => el.section.slug === 'homeSection1')?.section.subTitle}`,
    text: `${sections.find(el => el.section.slug === 'homeSection1')?.section.description}`,
  }

  const videoSrc = `${process.env.PUBLIC_URL}/Video/bg-${Date.now() % 2 === 0 ? 1 : 2}.mp4`
  const videoSrcMob = `${process.env.PUBLIC_URL}/Video/bg-1.mp4`

  const vidRef = useRef(null)

  const [isVideoPlaying, setVideoPlaying] = useState(true)

  useEffect(() => {
    if (vidRef.current && isIOS) {
        const promise = document.querySelector('video').play();

        if (promise !== undefined) {
          promise.catch(() => {
            setVideoPlaying(false)
          }).then(() => {
            if (vidRef.current && vidRef.current.paused) {
              setVideoPlaying(true)
            }
          });
        }
    }
  }, [])


  return isDesktop ? (
    <Layout
      className='home-section1'
      style={{
        background: `linear-gradient(0deg, rgba(6, 64, 72, 0.56), rgba(6, 64, 72, 0.56)), url(${backgroundSrc}) no-repeat`,
        backgroundSize: 'cover',
        width: '100%',
        overflow: 'hidden',
        position: 'relative',
        height: '100vh',
      }}
    >
      <Row className='overlay' wrap align={'middle'}>
        <Col span={24} style={{ alignItems: 'center', display: 'flex', flexDirection: 'column' }}>
          <Row align={'middle'} style={{flexDirection: 'column'}}>
            <Col md={22} lg={20} xl={18} xxl={16} className='section1-heading'>
              {item.heading}
            </Col>
            <Col md={20} lg={18} xl={16} xxl={12} className='section1-paragraph' style={{marginTop: 60}}>
              {item.paragraph}
            </Col>
          </Row>
        </Col>
        <Col sm={20} md={18} lg={15} xl={15} xxl={15} className='section1-text'>
          <span style={{ textAlign: 'start' }}>{item.text}</span>
          <span className='btn-wrapper'>
            <NavLink to={'/company'}>
              <Button type='link' ghost style={{ border: 'none', height: 'auto', padding: '0' }}>
                <Btn className='btn-active' />
              </Button>
            </NavLink>
          </span>
        </Col>
      </Row>
      <Row className='video-bg'>
        <video id='backgroundVideo' loop autoPlay playsInline muted className='fullscreen-bg__video'>
          <source src={videoSrc} type='video/mp4' />
        </video>
      </Row>
    </Layout>
  ) : (
    <Layout>
      <Row
        style={{
          background: `linear-gradient(0deg, rgba(6, 64, 72, 0.56), rgba(6, 64, 72, 0.56)), url(${backgroundSrc}) no-repeat`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          alignContent: 'center',
          overflow: 'hidden',
          position: 'relative',
          height: '100vh',
        }}
      >
        <Row wrap justify={'center'} align={'middle'} className='overlay-mob'>
          <Col span={22} className='section1-heading-mob'>
            {item.heading}
          </Col>
          <Col span={22} className='section1-paragraph-mob'>
            {item.paragraph}
          </Col>
        </Row>
        <Row style={{height: '100%', width: '100%'}}>
          <div className='video-bg-mob'>
            {isVideoPlaying ? (
              <video
                controls={false}
                src={videoSrcMob}
                ref={vidRef}
                playsInline={true}
                muted={true}
                loop={true}
                autoPlay={true}
                className='fullscreen-bg__video'
              />
            ) : null}
          </div>
        </Row>
      </Row>
      <Row style={{ background: '#E3D7CD;' }} className='section1-text-wrapper'>
        <Col span={24} className='section1-text-mob'>
          <Typography.Paragraph>{item.text}</Typography.Paragraph>
          <span className='btn-wrapper-mob'>
            <NavLink to={'/company'}>
              <Button type='link' ghost style={{ border: 'none', height: 'auto', padding: '0' }}>
                <Btn className='btn-active-mob' width={248} height={48} />
              </Button>
            </NavLink>
          </span>
        </Col>
      </Row>
    </Layout>
  )
}

export { Section1 }
