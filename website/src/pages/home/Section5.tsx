import { FC, ReactNode } from 'react'
import { Col, Layout, Row } from 'antd'
import './Section5.less'
import { useMediaQuery } from 'react-responsive'
import card1Small from './card1-small.png'
import card2Small from './card2-small.png'
import cardImg1 from './card1-section5.png'
import cardImg2 from './card2-section5.png'

type SectionProps = {
  heading1: ReactNode
  paragraph1: ReactNode
  heading2: ReactNode
  paragraph2: ReactNode
  paragraph3?: ReactNode
  src1: string
  src2: string
}
const Section5: FC = () => {
  const isDesktop = useMediaQuery({ minWidth: 768 })
  const isMobile = useMediaQuery({ maxWidth: 767 })
  const item = {
    heading1: 'Quality Above All Else',
    paragraph1:
      "Our customers come first. London Luxury's products are all designed with the consumer in mind, creating products with exceptional quality, that help to enhance the way people sleep, live and work.",
    heading2: 'Branding: A Fresh Approach',
    paragraph2:
      'London Luxury crafts aspirational narratives through differentiated product and design. We align with world-class brands that stand for real authenticity, beneficial health & wellness, and living life to the fullest - always striving for peace of mind, and enhancing overall wellness.',
    paragraph3:
      'We are entrusted as ambassadors with the brands that people love, and help to amplify them and make their voices heard in new, unexpected places that surprise and delight. Always providing the best, and most memorable experiences.',
    src1: cardImg1,
    src2: cardImg2,
  }
  const cards = [
    {
      srcSmall: card1Small,
      title: 'Quality Above All Else',
      paragraph1:
        "Our customers come first. London Luxury's products are all designed with the consumer in mind, creating products with exceptional quality, that help to enhance the way people sleep, live and work.",
      paragraph2: '',
    },
    {
      srcSmall: card2Small,
      title: 'Branding: A Fresh Approach',
      paragraph1:
        'London Luxury crafts aspirational narratives through differentiated product and design. We align with world-class brands that stand for real authenticity, beneficial health & wellness, and living life to the fullest - always striving for peace of mind, and enhancing overall wellness.',
      paragraph2:
        'We are entrusted as ambassadors with the brands that people love, and help to amplify them and make their voices heard in new, unexpected places that surprise and delight. Always providing the best, and most memorable experiences.',
    },
  ]
  return (
    <>
      {isDesktop && (
        <Layout
          style={{
            background: '#122222',
            width: '100%',
          }}
          className='section5-block-wrapper'
        >
          <Row wrap justify={'space-between'} className='section5 block'>
            <Col sm={24} md={24} lg={24} xl={12} xxl={12} className='section5-text'>
              <Col span={24} className='section5-heading'>
                {item.heading1}
              </Col>
              <Col span={24} className='section5-paragraph'>
                {item.paragraph1}
              </Col>
            </Col>
            <Col sm={24} md={24} lg={24} xl={12} xxl={12} className='section5-img1'>
              <img src={item.src1} alt='photo' style={{ width: '100%' }} />
            </Col>
          </Row>
          <Row wrap justify={'space-between'} className='section5 block2'>
            <Col sm={24} md={24} lg={24} xl={12} xxl={12} className='section5-img2'>
              <img src={item.src2} alt='photo' style={{ width: '100%' }} />
            </Col>
            <Col sm={24} md={24} lg={24} xl={12} xxl={12} className='section5-text2'>
              <Col span={24} className='section5-heading'>
                {item.heading2}
              </Col>
              <Col span={24} className='section5-paragraph2'>
                {item.paragraph2}
              </Col>
              <Col span={24} className='section5-paragraph2'>
                {item.paragraph3}
              </Col>
            </Col>
          </Row>
        </Layout>
      )}
      {isMobile && (
        <Layout
          style={{
            background: '#122222',
            width: '100%',
            paddingTop: '72px',
            paddingBottom: '56px',
          }}
        >
          <Row wrap justify={'center'} align={'middle'} style={{ marginBottom: '64px' }}>
            <Col span={20} className='section5-img1-mob'>
              <img src={cards[0].srcSmall} alt='photo' style={{ width: '100%' }} />
            </Col>
            <Col span={20} className='section5-text-mob'>
              <Col span={24} className='section5-heading-mob'>
                {cards[0].title}
              </Col>
              <Col span={24} className='section5-paragraph-mob'>
                {cards[0].paragraph1}
              </Col>
            </Col>
          </Row>
          <Row wrap justify={'center'} align={'middle'}>
            <Col span={20} className='section5-img2-mob'>
              <img src={cards[1].srcSmall} alt='photo' style={{ width: '100%' }} />
            </Col>
            <Col span={20} className='section5-text-mob'>
              <Col span={20} offset={2} className='section5-heading-mob'>
                {cards[1].title}
              </Col>
              <Col span={24} className='section5-paragraph-mob'>
                {cards[1].paragraph1}
              </Col>
              <Col span={24} className='section5-paragraph-mob'>
                {cards[1].paragraph2}
              </Col>
            </Col>
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section5 }
