import { FC, ReactNode } from 'react'
import { Col, Layout, Row } from 'antd'
import Slider from '../../components/carousel/Slider'
import ItemSlider, { ItemSliderProps } from '../../components/carousel/ItemSlider'
import img7 from '../../components/carousel/images/img7-home-page.png'
import img8 from '../../components/carousel/images/img8-home-page.png'
import slide1Small from './slide1-section6.png'
import slide2Small from './slide2-section6.png'
import SampleNextArrow from '../../components/carousel/SampleNextArrow'
import SamplePrevArrow from '../../components/carousel/SamplePrevArrow'
import { useMediaQuery } from 'react-responsive'

type SectionProps = { paragraph: ReactNode }

const Section6: FC<SectionProps> = ({ paragraph }) => {
  const SliderElement = (props: Partial<ItemSliderProps>) => (
    <ItemSlider paragraph={paragraph} className='text-wrapper' {...props} />
  )

  const images: Partial<ItemSliderProps>[] = [
    { src: img7, title: 'World’s leading manufacturer of allergen-barrier consumer products.', number: '01', id: 1 },
    { src: img8, title: 'Largest U.S. manufacturer and importer of Turkish Cotton for consumer bath products.', number: '02', id: 2 },
    {
      src: img7,
      title:
        'Produce over 100 million bath towels, slippers and robes, pillows, sheets, blankets and consumer health and wellness each year.',
      number: '03',
      id: 3,
    },
  ]
  const isDesktop = useMediaQuery({ minWidth: 768 })
  const isMobile = useMediaQuery({ maxWidth: 767 })

  const imagesMob: Partial<ItemSliderProps>[] = [
    {
      src: slide1Small,
      title: 'World’s leading manufacturer of allergen-barrier consumer products.',
      number: '01',
      id: 1,
    },
    {
      src: slide2Small,
      title: 'Largest U.S. manufacturer and importer of Turkish Cotton for consumer bath products.',
      number: '02',
      id: 2,
    },
    {
      src: slide1Small,
      title:
        'Produce over 100 million bath towels, slippers and robes, pillows, sheets, blankets and consumer health and wellness each year.',
      number: '03',
      id: 3,
    },
  ]
  const settings = {
    fade: true,
    dots: false,
    infinite: true,
    speed: 2000,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    // autoplay: true,
    autoplaySpeed: 5000,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          fade: false,
          dots: false,
          infinite: true,
          speed: 1500,
          arrows: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 0,
          swipeToSlide: true,
          autoplaySpeed: 5000,
          nextArrow: <SampleNextArrow />,
          prevArrow: <SamplePrevArrow />,
          cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
        },
      },
    ],
  }
  return (
    <>
      {isDesktop && (
        <Layout
          style={{
            width: '100%',
          }}
        >
          <Row wrap>
            <Col span={24}>
              <Slider settings={settings} wrapperClassName='slider-section6'>
                {images.map(SliderElement)}
              </Slider>
            </Col>
          </Row>
        </Layout>
      )}
      {isMobile && (
        <Layout
          style={{
            width: '100%',
            height: '606px',
          }}
        >
          <Row wrap>
            <Col span={24}>
              <Slider settings={settings} wrapperClassName='slider-section6-mob'>
                {imagesMob.map(SliderElement)}
              </Slider>
            </Col>
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section6 }
