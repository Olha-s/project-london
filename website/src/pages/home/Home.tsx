import { FC, useEffect } from 'react'
import { Layout } from 'antd'
import { Section1 } from './Section1'
import { Section2 } from './Section2'
import { Section3 } from './Section3'
import { Section5 } from './Section5'
import { Section6 } from './Section6'
import { SectionMap } from '../../components/map/SectionMap'
import { Section4 } from './Section4'
import {trademarkReduce} from '../../containers/index'

const Home: FC = () => {
    useEffect(() => {
      trademarkReduce(['section3-paragraph', 'section3-paragraph-mob'])
    }, [])
    return (
      <Layout>
          <Section1 />
          <Section2 />
          <Section3 />
          <Section4 />
          <Section5 />
          <Section6 paragraph='Our Key Stats' />
          <SectionMap />
      </Layout>
    )
}

export { Home }
