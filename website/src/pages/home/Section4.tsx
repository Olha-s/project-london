import { FC, ReactNode, useRef } from 'react'
import { Col, Layout, Menu, Row } from 'antd'
import './Section4.less'
import { useMediaQuery } from 'react-responsive'
import ItemSlider, { ItemSliderProps } from '../../components/carousel/ItemSlider'
import { ReactComponent as ArrowBtn } from './arrow-btn.svg'
import SampleNextArrow from '../../components/carousel/SampleNextArrow'
import SamplePrevArrow from '../../components/carousel/SamplePrevArrow'
import Slider from '../../components/carousel/Slider'
import { useContent } from '../../containers/CMS'
import { NavLink, Link } from 'react-router-dom'

const Section4: FC = () => {
  const SliderElement = (props: Partial<ItemSliderProps>) => (
    <ItemSlider className='home-section4-text-wrapper-mob' {...props} />
  )
  const content = useContent()
  // const onClick = (myRef:any) => {
  //   window.scrollTo(0, myRef.current.scrollHeight)
  //   console.log( current.scrollHeight)
  // }
  const isDesktop = useMediaQuery({ minWidth: 992 })
  const isMobile = useMediaQuery({ maxWidth: 991 })
  const cards = [
    {
      title: 'Global Manufacturer, Design Development and Sourcing Power House',
      number: '01',
      id: 1,
    },
    {
      title: 'Full-Service Omni Channel Marketing & Advertising Experts – In house and Under One Roof',
      number: '02',
      id: 2,
    },
    {
      title: 'In Depth Knowledge of Our Community',
      number: '03',
      id: 3,
    },
    {
      title: 'Infrastructure Engineered for Maximum Efficiency',
      number: '04',
      id: 4,
    },
  ]

  const settings = {
    dots: false,
    fade: true,
    infinite: true,
    speed: 1500,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    // autoplay: true,
    autoplaySpeed: 5000,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    //adaptiveHeight: true,
  }
  return (
    <>
      {isDesktop && (
        <Layout
          style={{
            background: '#091513',
            width: '100%',
          }}
        >
          <Row wrap justify={'center'} align={'middle'} className='home-section4-block'>
            <Col
              span={20}
              style={{
                height: '61%',
                position: 'absolute',
                top: '0',
                left: '0',
                background: '#122222',
                width: '80.1%',
                maxWidth: '1870px',
              }}
            />
            <Col
              span={21}
              md={23}
              lg={23}
              xl={23}
              xxl={22}
              style={{ display: 'flex', justifyContent: 'space-between', maxWidth: '1650px' }}
              className='home-section4-wrapper-card'
            >
              {cards.map(el => (
                <div key={el.id} className='home-section4-wrapper-item'>
                  <div className='home-section4-wrapper'>
                    <div className='home-section4-title'>{el.title}</div>

                    {/*  <NavLink to={'/company/#info'} >*/}
                    {/*<span className='arrow-btn-wrapper' >*/}
                    {/*   <ArrowBtn width={'109.94px'} className='arrow-btn' />*/}
                    {/*  </span>*/}
                    {/*  </NavLink>*/}
                  </div>
                </div>
              ))}
            </Col>
          </Row>
        </Layout>
      )}

      {isMobile && (
        <Layout
          style={{
            background: '#091513',
            width: '100%',
          }}
        >
          <Row wrap justify={'center'} align={'middle'} className='home-section4-block-mob'>
            <Col span={22} style={{ paddingTop: '80px', paddingBottom: '80px' }}>
              <Slider settings={settings} wrapperClassName='home-section4-block-mob'>
                {cards.map(SliderElement)}
              </Slider>
            </Col>
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section4 }
