import { FC, ReactNode, useEffect, useRef, useState } from 'react'
import { Carousel, Col, Layout, Row } from 'antd'
import './Section2.less'
import img1 from '../../components/carousel/images/img1-home-page.png'
import img2 from '../../components/carousel/images/img2-home-page.png'
import img3 from '../../components/carousel/images/img3-home-page.png'
import img4 from '../../components/carousel/images/img4-home-page.png'
import img5 from '../../components/carousel/images/img5-home-page.png'
import img6 from '../../components/carousel/images/img6-home-page.png'
import SampleNextArrow from '../../components/carousel/SampleNextArrow'
import SamplePrevArrow from '../../components/carousel/SamplePrevArrow'
import { useMediaQuery } from 'react-responsive'
import { useContent } from '../../containers/CMS'
import ReactMarkdown from 'react-markdown'
import '../../components/carousel/Slider.less'
import '../../components/carousel/ItemSlider.less'

const Section2: FC = () => {
  const content = useContent()
  const item = {
    heading: `${content?.website.sections.find(el => el.section.slug === 'homeSection2')?.section.title}`,
    text: `${content?.website.sections.find(el => el.section.slug === 'homeSection2')?.section.description}`,
  }
  const isDesktop = useMediaQuery({ minWidth: 992 })
  const isMobile = useMediaQuery({ maxWidth: 991 })

  const images = [img6, img1, img2, img3, img4, img5]
  const imagesMob = [img1, img2, img3, img4, img5, img6]

  const texts = [
    'The combined weight of everything we made in 2020 would be the equivalent of ***8,000*** fully grown African elephants, or 96,000,000 lbs.',
    'Employees at London Luxury proudly represent ***48*** nationalities.',
    'All pillows we’ve made, stacked on top of each other, would be over ***400*** miles high, or would extend to reach the oceans of water deep in the earth’s mantle, 400 miles below the surface.',
    'The square feet of fabric woven last year would cover every football field in the ***NFL…20*** times over, equaling 40,000,000 square feet.',
    'Total market cap of London Luxury brand partners: over ***$200*** Billion.',
    'Linear feet of towels we make appearing in retail stores: ***14*** miles – as long as the island of Manhattan. ',
  ]

  const settings = {
    dots: false,
    infinite: true,
    speed: 1500,
    arrows: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    //autoplay: true,
    autoplaySpeed: 5000,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    // cssEase: 'cubic-bezier(0.7, 0, 0.3, 0.3)',
  }
  const settingText = {
    dots: false,
    infinite: true,
    fade: true,
    speed: 1500,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 0.3)',
  }
  const settings2 = {
    dots: false,
    infinite: true,
    speed: 1500,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    autoplay: false,
    autoplaySpeed: 5000,
    adaptiveHeight: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 0.3)',
    // fade: true,
  }

  const settingsMob = {
    dots: false,
    infinite: true,
    speed: 1500,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    autoplay: false,
    autoplaySpeed: 5000,
   // adaptiveHeight: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    // cssEase: 'cubic-bezier(0.7, 0, 0.3, 0.3)',
  }
  const settingTextMob = {
    dots: false,
    infinite: true,
    fade: true,
    speed: 1500,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 0.3)',
  }

  const [nav1, setNav1] = useState(null)
  const [nav2, setNav2] = useState(null)
  const slider1 = useRef(null)
  const slider2 = useRef(null)
  useEffect(() => {
    setNav1(slider1.current)
    setNav2(slider2.current)
  }, [slider1, slider2])
  return (
    <>
      {isDesktop && (
        <Layout
          style={{
            background: '#0D1B1C',
            width: '100%',
            alignItems: 'center',
            overflow: 'hidden',
            position: 'relative',
          }}
          className='home-section2-wrapper'
        >
          <Row
            wrap
            style={{
              height: '100%',
              maxWidth: '1920px',
            }}
          >
            <Col md={18} lg={18} xl={16} xxl={16}>
              <div className='slider-section2'>
                <Carousel asNavFor={nav2} ref={slider1} {...settings}>
                  {images.map((slide, index) => (
                    <div className='item-wrapper' key={index}>
                      <img src={slide} alt='photo' />
                    </div>
                  ))}
                </Carousel>
              </div>
            </Col>
            <Col md={6} lg={6} xl={8} xxl={7} className='section2-heading'>
              {item.heading}
            </Col>

            <Col md={8} lg={8} xl={10} xxl={11} className='section2-text'>
              <Carousel {...settingText} asNavFor={nav1} ref={slider2} className='home-slider2-section2'>
                {texts.map((slide, index) => (
                  <div className='slider2-section2-item-wrapper' key={index}>
                    <ReactMarkdown>{slide}</ReactMarkdown>
                  </div>
                ))}
              </Carousel>
            </Col>
          </Row>
        </Layout>
      )}
      {isMobile && (
        <Layout
          style={{
            background: '#091513',
            width: '100%',
            paddingTop: '32px',
          }}
        >
          <Row
            wrap
            style={{
              height: '100%',
              position: 'relative',
              background: '#091513',
            }}
          >
            <Col span={24} className='section2-heading-mob'>
              {item.heading}
            </Col>
            <Col span={24}>
              <div className='slider-section2-mob'>
                <Carousel asNavFor={nav2} ref={slider1} {...settingsMob}>
                  {imagesMob.map((slide, index) => (
                    <div className='item-wrapper' key={index}>
                      <img src={slide} alt='photo' />
                    </div>
                  ))}
                </Carousel>
              </div>
            </Col>
            <Col span={24} className='section2-text-mob'>
              <Carousel {...settingTextMob} asNavFor={nav1} ref={slider2}>
                {texts.map((slide, index) => (
                  <div key={index} className='section2-slider-text-mob'>
                    <ReactMarkdown>{slide?.toString() as string}</ReactMarkdown>
                  </div>
                ))}
              </Carousel>
            </Col>
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section2 }
