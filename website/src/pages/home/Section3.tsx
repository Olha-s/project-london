import { FC, ReactNode } from 'react'
import { Button, Col, Layout, Row } from 'antd'
import backgroundSrc from './bg-section3.png'
import './Section3.less'
import { useMediaQuery } from 'react-responsive'
import { useContent } from '../../containers/CMS'

const Section3: FC = () => {
  const content = useContent()
  const isDesktop = useMediaQuery({ minWidth: 768 })
  const isMobile = useMediaQuery({ maxWidth: 767 })

  const item = {
    heading: `${content?.website.sections.find(el => el.section.slug === 'homeSection3')?.section.title}`,
    paragraph: `${content?.website.sections.find(el => el.section.slug === 'homeSection3')?.section.description}`,
  }

  return (
    <>
      {isDesktop && (
        <Layout
          style={{
            background: `linear-gradient(0deg, rgba(10, 35, 38, 0.76), rgba(10, 35, 38, 0.76)), url(${backgroundSrc}) no-repeat`,
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            width: '100%',
            paddingTop: '63.5px',
            paddingBottom: '92.5px',
          }}
        >
          <Row
            wrap
            justify={'center'}
            style={{
              height: '100%',
            }}
          >
            <Col md={18} lg={18} xl={17} xxl={16} className='section3-block'>
              <Col span={18} offset={3} className='section3-heading'>
                {item.heading}
              </Col>
              <Col span={18} offset={3} className='section3-paragraph'>
                {item.paragraph}
              </Col>
            </Col>
          </Row>
        </Layout>
      )}

      {isMobile && (
        <Layout
          style={{
            background: `url(${backgroundSrc}) no-repeat`,
            backgroundSize: 'cover',
            height: '606px',
            width: '100%',
            backgroundPosition: 'center',
            alignContent: 'center',
          }}
        >
          <Row
            wrap
            justify={'center'}
            align={'middle'}
            style={{
              height: '100%',
            }}
          >
            <Col span={20} className='section3-block-mob'>
              <Col span={24} className='section3-heading-mob'>
                {item.heading}
              </Col>
              <Col span={24} className='section3-paragraph-mob'>
                {item.paragraph}
              </Col>
            </Col>
          </Row>
        </Layout>
      )}
    </>
  )
}

export { Section3 }
