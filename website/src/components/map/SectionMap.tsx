import { FC } from 'react'
import { Col, Layout } from 'antd'
import mapSmall from './map-small.png'
import './SectionMap.less'
import map from './images/map.png'
import are from './images/are.png'
import britania from './images/britania.png'
import chn from './images/chn.png'
import columbia from './images/columbia.png'
import ind from './images/ind.png'
import izr from './images/izr.png'
import pak from './images/pak.png'
import portugali from './images/portugali.png'
import turkish from './images/turkish.png'
import usa from './images/usa.png'

const SectionMap: FC = () => {

  const item = [{ src: mapSmall, title: 'Our offices' }]
  return (
    <>
        <Layout className='map'>
          <Col span={24} className='map-heading'>
            {item[0].title}
          </Col>

          <div className='map__wrap'>
            <button
              tabIndex={-1}
              className='city top'
              data-coords='199|293'
              data-position='bottom'
              style={{ top: '43.4768%', left: '13.7907%' }}
            >
              <div className='triangle'></div>
              <div className='city-info'>
                <div className='flag'>
                  <img src={usa} alt='' />
                </div>
                <div className='city-name'>Los Angeles</div>
              </div>
              <span className='point'></span>
            </button>
            <button
              tabIndex={-1}
              className='city top'
              data-coords='270.93|239.33'
              data-position='bottom'
              style={{ top: '35.5057%', left: '18.7755%' }}
            >
              <div className='triangle'></div>
              <div className='city-info'>
                <div className='flag'>
                  <img src={usa} alt='' />
                </div>
                <div className='city-name'>Bentonville</div>
              </div>
              <span className='point'></span>
            </button>
            <button
              tabIndex={-1}
              className='city top'
              data-coords='318.68|234.45'
              data-position='bottom'
              style={{ top: '34.7818%', left: '22.0845%' }}
            >
              <div className='triangle'></div>
              <div className='city-info'>
                <div className='flag'>
                  <img src={usa} alt='' />
                </div>
                <div className='city-name'>Milwaukee</div>
              </div>
              <span className='point'></span>
            </button>
            <button
              tabIndex={-1}
              className='city top'
              data-coords='362.53|266.61'
              data-position='bottom'
              style={{ top: '39.5528%', left: '25.1233%' }}
            >
              <div className='triangle'></div>
              <div className='city-info'>
                <div className='flag'>
                  <img src={usa} alt='' />
                </div>
                <div className='city-name'>New York</div>
              </div>
              <span className='point'></span>
            </button>
            <button
              tabIndex={-1}
              className='city top'
              data-coords='335.25|260.76'
              data-position='bottom'
              style={{ top: '38.6850%', left: '23.2328%' }}
            >
              <div className='triangle'></div>
              <div className='city-info'>
                <div className='flag'>
                  <img src={usa} alt='' />
                </div>
                <div className='city-name'>Edison</div>
              </div>
              <span className='point'></span>
            </button>
            <button
              tabIndex={-1}
              className='city top'
              data-coords='329.4|285.12'
              data-position='bottom'
              style={{ top: '42.299%', left: '22.8274%' }}
            >
              <div className='triangle'></div>
              <div className='city-info'>
                <div className='flag'>
                  <img src={usa} alt='' />
                </div>
                <div className='city-name'>Charlotte</div>
              </div>
              <span className='point'></span>
            </button>
            <button
              tabIndex={-1}
              className='city top'
              data-coords='351.81|330.92'
              data-position='bottom'
              style={{ top: '49.0935%', left: '24.3804%' }}
            >
              <div className='triangle'></div>
              <div className='city-info'>
                <div className='flag'>
                  <img src={usa} alt='' />
                </div>
                <div className='city-name'>Miami</div>
              </div>
              <span className='point'></span>
            </button>
            <button
              tabIndex={-1}
              className='city top'
              data-coords='389.81|419.59'
              data-position='bottom'
              style={{ top: '62.2482%', left: '27.0139%' }}
            >
              <div className='triangle'></div>
              <div className='city-info'>
                <div className='flag'>
                  <img src={columbia} alt='' />
                </div>
                <div className='city-name'>Bogota</div>
              </div>
              <span className='point'></span>
            </button>
            <button
              tabIndex={-1}
              className='city top'
              data-coords='662.65|201.34'
              data-position='bottom'
              style={{ top: '29.8697%', left: '45.9217%' }}
            >
              <div className='triangle'></div>
              <div className='city-info'>
                <div className='flag'>
                  <img src={britania} alt='' />
                </div>
                <div className='city-name'>London</div>
              </div>
              <span className='point'></span>
            </button>
            <button
              tabIndex={-1}
              className='city top'
              data-coords='634.39|254.92'
              data-position='bottom'
              style={{ top: '37.8186%', left: '43.9633%' }}
            >
              <div className='triangle'></div>
              <div className='city-info'>
                <div className='flag'>
                  <img src={portugali} alt='' />
                </div>
                <div className='city-name'>Porto</div>
              </div>
              <span className='point'></span>
            </button>
            <button
              tabIndex={-1}
              className='city top'
              data-coords='805.89|266.61'
              data-position='bottom'
              style={{ top: '39.5528%', left: '55.8482%' }}
            >
              <div className='triangle'></div>
              <div className='city-info'>
                <div className='flag'>
                  <img src={turkish} alt='' />
                </div>
                <div className='city-name'>Istanbul</div>
              </div>
              <span className='point'></span>
            </button>
            <button
              tabIndex={-1}
              className='city top'
              data-coords='816.61|294.87'
              data-position='bottom'
              style={{ top: '43.7454%', left: '56.5911%' }}
            >
              <div className='triangle'></div>
              <div className='city-info'>
                <div className='flag'>
                  <img src={izr} alt='' />
                </div>
                <div className='city-name'>Tel Aviv</div>
              </div>
              <span className='point'></span>
            </button>
            <button
              tabIndex={-1}
              className='city top'
              data-coords='899.44|302.2'
              data-position='bottom'
              style={{ top: '44.8328%', left: '62.3312%' }}
            >
              <div className='triangle'></div>
              <div className='city-info'>
                <div className='flag'>
                  <img src={are} alt='' />
                </div>
                <div className='city-name'>Dubai</div>
              </div>
              <span className='point'></span>
            </button>
            <button
              tabIndex={-1}
              className='city top'
              data-coords='964.72|302.2'
              data-position='bottom'
              style={{ top: '44.8328%', left: '66.8552%' }}
            >
              <div className='triangle'></div>
              <div className='city-info'>
                <div className='flag'>
                  <img src={pak} alt='' />
                </div>
                <div className='city-name'>Karachi</div>
              </div>
              <span className='point'></span>
            </button>
            <button
              tabIndex={-1}
              className='city top'
              data-coords='1020.27|302.2'
              data-position='bottom'
              style={{ top: '44.8328%', left: '70.7048%' }}
            >
              <div className='triangle'></div>
              <div className='city-info'>
                <div className='flag'>
                  <img src={ind} alt='' />
                </div>
                <div className='city-name'>New Delhi</div>
              </div>
              <span className='point'></span>
            </button>
            <button
              tabIndex={-1}
              className='city top'
              data-coords='1137.2|284.15'
              data-position='bottom'
              style={{ top: '42.155%', left: '78.8080%' }}
            >
              <div className='triangle'></div>
              <div className='city-info'>
                <div className='flag'>
                  <img src={chn} alt='' />
                </div>
                <div className='city-name'>Shanghai</div>
              </div>
              <span className='point'></span>
            </button>

            <img className='img-map' src={map} alt='map' />
          </div>
        </Layout>
    </>
  )
}

export { SectionMap }
