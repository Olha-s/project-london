import { FC } from 'react'
import './Slider.less'
import { ReactComponent as ArrowNext } from './images/arrow-next.svg'

type ArrowProps = {
  onClick?: () => void
}
const SampleNextArrow: FC<ArrowProps> = ({ onClick }) => {
  return (
    <div className='arrow-next' onClick={onClick}>
      <ArrowNext />
    </div>
  )
}

export default SampleNextArrow
