import { FC } from 'react'
import './Slider.less'
import { ReactComponent as ArrowPrev } from './images/arrow-prev.svg'

type ArrowProps = {
  onClick?: () => void
}
const SamplePrevArrow: FC<ArrowProps> = ({ onClick }) => {
  return (
    <div className='arrow-prev' onClick={onClick}>
      <ArrowPrev />
    </div>
  )
}

export default SamplePrevArrow
