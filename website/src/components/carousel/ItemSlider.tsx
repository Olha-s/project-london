import { FC, ReactNode } from 'react'
import './ItemSlider.less'

export type ItemSliderProps = {
  src?: string
  title?: ReactNode
  number?: ReactNode
  paragraph?: ReactNode
  className?: string
  num?: string
  paragraph1?: ReactNode
  paragraph2?: ReactNode
  text?: ReactNode
  heading?: ReactNode
  srcNum?: string
  srcImg?: string
  id?: string | number
  srcAct?: string
}

const ItemSlider: FC<Partial<ItemSliderProps>> = ({
  src,
  title,
  number,
  paragraph,
  className,
  num,
  paragraph1,
  paragraph2,
  text,
  srcNum,
  srcImg,
  srcAct,
  id,
}) => {
  return (
    <div className='item-wrapper' key={id}>
      {srcNum && (
        <div className='operations-number'>
          <img src={srcNum} alt='photo' />
        </div>
      )}
      {srcAct && (
        <div className='img-srcAct'>
          <img src={srcAct} alt='photo' />
        </div>
      )}
      {src && <img src={src} alt='photo' />}
      {num && (
        <div className='img-number'>
          <img src={num} alt='photo' />
        </div>
      )}
      <div className={className}>
        {paragraph && <div className='item-paragraph'>{paragraph}</div>}
        {title && <div className='item-title'> <p>{title}</p> </div>}
        {number && <div className='item-number'>{number}</div>}
        {paragraph1 && <div className='item-paragraph1'>{paragraph1}</div>}
        {paragraph2 && <div className='item-paragraph2'>{paragraph2}</div>}
        {text && <div className='operations-text'>{text}</div>}
      </div>
      {srcImg && (
        <div className='operations-img'>
          <img src={srcImg} alt='photo' />
        </div>
      )}
    </div>
  )
}
export default ItemSlider
