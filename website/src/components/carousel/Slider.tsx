import { FC } from 'react'
import { Carousel } from 'antd'
import './Slider.less'

type SliderProps = {
  settings: typeof Carousel.defaultProps
  wrapperClassName: string
}

const Slider: FC<SliderProps> = ({ wrapperClassName = '', settings, children }) => (
  <div className={wrapperClassName}>
    <Carousel {...settings}>{children}</Carousel>
  </div>
)

export default Slider
