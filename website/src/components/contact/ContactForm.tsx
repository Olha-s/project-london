import { FC } from 'react'
import { Button, Form, FormProps, Input, Layout, message, Upload, Typography, Checkbox } from 'antd'
import { ValidateMessages } from 'rc-field-form/lib/interface'
import { useCreateContactFormRequestMutation, ContactFormRequestInput } from '../../graphql'
import './ContactForm.less'
import { ReactComponent as DownloadIcon } from './download.svg'
import { useMediaQuery } from 'react-responsive'

const formProps: FormProps = {
  labelCol: { span: 8 },
  wrapperCol: { span: 8 },
}

const validateMessages: ValidateMessages = {
  required: '${name} is required!',
  types: {
    email: '${name} is not a valid email!',
  },
}

type ContactFormProps = Partial<{
  onFormFinish: (values: ContactFormRequestInput) => void
  notificationContext: JSX.Element
}>

function withBackend(Wrapped: FC<Partial<ContactFormProps>>): FC<Partial<ContactFormProps>> {
  return process.env.REACT_APP_API_URL
    ? ({ ...props }) => {
        const messageKey = 'request'
        const [createContactFormRequest] = useCreateContactFormRequestMutation()
        const [api, contextHolder] = message.useMessage()
        message.config({
          duration: 1,
          maxCount: 3,
          prefixCls: 'app',
        })
        const onFinish = (data: ContactFormRequestInput) => {
          api
            .loading(
              {
                content: 'Send your request ...',
                key: messageKey,
              },
              0,
            )
            .then(() =>
              createContactFormRequest({ variables: { input: { data } } })
                .then(result =>
                  result?.data
                    ? api.success(
                        {
                          content: `Thanks for your interest, ${result.data.createContactFormRequest.contactFormRequest.id}! Your message received, ${result.data.createContactFormRequest.contactFormRequest.id}`,
                          key: messageKey,
                        },
                        2000,
                      )
                    : api.error(
                        {
                          content: `Error sending your request: ${result?.errors.join('\n')}`,
                          key: messageKey,
                        },
                        2000,
                      ),
                )
                .catch(reason =>
                  api
                    .error(
                      {
                        content: `Error sending your request: ${reason}`,
                        key: messageKey,
                      },
                      2000,
                    )
                    .then(() => message.destroy(messageKey)),
                )
                .finally(() => message.destroy(messageKey)),
            )
        }
        return (
          <Wrapped
            notificationContext={contextHolder}
            onFormFinish={process.env.REACT_APP_API_URL ? undefined : onFinish}
            {...props}
          />
        )
      }
    : ({ ...props }) => <Wrapped {...props} />
}

const ContactFormHeadless: FC<ContactFormProps> = ({ onFormFinish, notificationContext }) => {
  const isDesktop = useMediaQuery({ minWidth: 769 })
  const [form] = Form.useForm<ContactFormRequestInput & { subscribe: boolean }>()
  return isDesktop ? (
    <Layout style={{ background: '#F3EDE8' }}>
      {notificationContext}
      <Typography.Paragraph
        className='title-contact-form'
        style={{
          marginBottom: '48px',
        }}
      >
        Write Us a Message
      </Typography.Paragraph>
      <Form
        form={form}
        {...formProps}
        name='contactFormRequest'
        onFinish={onFormFinish}
        validateMessages={validateMessages}
      >
        <div style={{ display: 'flex' }}>
          <Form.Item
            name={'firstName'}
            rules={[{ required: true, message: 'Please input your first name!' }]}
            style={{ width: '50%', paddingRight: '16px' }}
            wrapperCol={{ span: '24' }}
          >
            <Input placeholder='First Name *' className='input-form' />
          </Form.Item>
          <Form.Item
            name={'lastName'}
            rules={[{ required: true, message: 'Please input your last name!' }]}
            style={{ width: '50%', paddingLeft: '16px' }}
            wrapperCol={{ span: '24' }}
          >
            <Input placeholder='Last Name *' className='input-form' />
          </Form.Item>
        </div>

        <div style={{ display: 'flex' }}>
          <Form.Item
            name={'email'}
            rules={[
              { type: 'email', message: 'Email not a valid email!' },
              {
                required: true,
                message: 'Please input your email!',
              },
            ]}
            style={{ width: '50%', paddingRight: '16px' }}
            wrapperCol={{ span: '24' }}
          >
            <Input placeholder='Email *' className='input-form' />
          </Form.Item>
          <Form.Item name={'phone'} style={{ width: '50%', paddingLeft: '16px' }} wrapperCol={{ span: '24' }}>
            <Input className='input-form' placeholder='Phone Number' />
          </Form.Item>
        </div>

        <div style={{ display: 'flex' }}>
          <Form.Item name={'company'} style={{ width: '50%', paddingRight: '16px' }} wrapperCol={{ span: '24' }}>
            <Input placeholder='Company' className='input-form' />
          </Form.Item>
          <Form.Item name={'source'} style={{ width: '50%', paddingLeft: '16px' }} wrapperCol={{ span: '24' }}>
            <Input className='input-form' placeholder='How Did You Hear About Us?' />
          </Form.Item>
        </div>
        <Form.Item
          name={'message'}
          rules={[{ required: true, message: 'Please input your message!' }]}
          wrapperCol={{ span: '24' }}
        >
          <Input.TextArea className='input-form' placeholder='Your Message *' />
        </Form.Item>

        <Form.Item name={'files'} wrapperCol={{ span: '24' }}>
          <Upload name={'files'} action={'http://localhost:8080/upload'}>
            <span style={{ display: 'flex', alignItems: 'center' }}>
              <Button shape='circle' icon={<DownloadIcon />} className='upload-btn' />
              <span className='upload-title'>Add an attachment </span>
              <span className='upload-subtitle'>(max file size 10MB; allowed extensions: doc, txt, pdf, docx)</span>
            </span>
          </Upload>
        </Form.Item>

        <Form.Item
          name='subscribe'
          valuePropName='checked'
          rules={[
            {
              validator: (_, value) =>
                value ? Promise.resolve() : Promise.reject(new Error('Should accept Privacy Policy')),
            },
          ]}
          wrapperCol={{ span: '24' }}
        >
          <Checkbox defaultChecked={true}>
            <span className='text-checkbox'>I want to receive news and updates once in a while</span>
            <div className='text-paragraph'>
              We will add your info to our CRM for contacting you regarding your request.
              <div>
                {' '}
                For more information please view our{' '}
                <a href='' className='link-checkbox'>
                  Privacy Policy
                </a>
              </div>
            </div>
          </Checkbox>
        </Form.Item>

        <Form.Item wrapperCol={{ span: '9' }} style={{ paddingRight: '26px', marginBottom: '0' }}>
          <Button htmlType='submit' className='submit-btn'>
            Send Message
          </Button>
        </Form.Item>
      </Form>
    </Layout>
  ) : (
    <Layout style={{ background: '#F3EDE8' }}>
      {notificationContext}
      <Typography.Paragraph className='title-contact-form-mob'>Write Us a Message</Typography.Paragraph>
      <Form form={form} name='contactFormRequest' onFinish={onFormFinish} validateMessages={validateMessages}>
        <Form.Item
          name={'firstName'}
          rules={[{ required: true, message: 'Please input your first name!' }]}
          wrapperCol={{ span: '24' }}
        >
          <Input placeholder='First Name *' className='input-form' />
        </Form.Item>
        <Form.Item
          name={'lastName'}
          rules={[{ required: true, message: 'Please input your last name!' }]}
          wrapperCol={{ span: '24' }}
        >
          <Input placeholder='Last Name *' className='input-form' />
        </Form.Item>
        <Form.Item
          name={'email'}
          rules={[
            { type: 'email', message: 'Email not a valid email!' },
            {
              required: true,
              message: 'Please input your email!',
            },
          ]}
          wrapperCol={{ span: '24' }}
        >
          <Input placeholder='Email *' className='input-form' />
        </Form.Item>
        <Form.Item name={'phone'} wrapperCol={{ span: '24' }}>
          <Input className='input-form' placeholder='Phone Number' />
        </Form.Item>
        <Form.Item name={'company'} wrapperCol={{ span: '24' }}>
          <Input placeholder='Company' className='input-form' />
        </Form.Item>
        <Form.Item name={'source'} wrapperCol={{ span: '24' }}>
          <Input className='input-form' placeholder='How Did You Hear About Us?' />
        </Form.Item>
        <Form.Item
          name={'message'}
          rules={[{ required: true, message: 'Please input your message!' }]}
          wrapperCol={{ span: '24' }}
        >
          <Input.TextArea className='input-form' placeholder='Your Message *' />
        </Form.Item>

        <Form.Item name={'files'} wrapperCol={{ span: '24' }}>
          <Upload name={'files'} action={'http://localhost:8080/upload'}>
            <span style={{ display: 'flex', alignItems: 'center' }}>
              <Button shape='circle' icon={<DownloadIcon />} className='upload-btn' />
              <span style={{ display: 'flex', flexDirection: 'column', marginLeft: '9px' }}>
                <Typography.Paragraph className='upload-title-mob' style={{ marginBottom: '0' }}>
                  Add an attachment{' '}
                </Typography.Paragraph>
                <Typography.Paragraph className='upload-subtitle-mob' style={{ marginBottom: '0' }}>
                  (max file size 10MB allowed)
                </Typography.Paragraph>
              </span>
            </span>
          </Upload>
        </Form.Item>

        <Form.Item
          name='Privacy Policy'
          valuePropName='checked'
          rules={[
            {
              validator: (_, value) =>
                value ? Promise.resolve() : Promise.reject(new Error('Should accept Privacy Policy')),
            },
          ]}
          wrapperCol={{ span: '24' }}
        >
          <Checkbox defaultChecked={true} className='checkbox-mob'>
            <span className='text-checkbox-mob' style={{ paddingLeft: '8px' }}>
              I want to receive news and updates once in a while
            </span>
            <div className='text-paragraph-mob' style={{ paddingTop: '8px', marginLeft: '-15px' }}>
              We will add your info to our CRM for contacting you regarding your request. For more information please
              view our{' '}
              <a href='' className='link-checkbox'>
                Privacy Policy
              </a>
            </div>
          </Checkbox>
        </Form.Item>

        <Form.Item wrapperCol={{ span: '24' }} style={{ marginBottom: '48px' }}>
          <Button htmlType='submit' className='submit-btn'>
            Send Message
          </Button>
        </Form.Item>
      </Form>
    </Layout>
  )
}

const ContactForm = withBackend(ContactFormHeadless)
export { ContactForm }
