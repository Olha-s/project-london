'use strict'

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    async afterCreate(data) {
      await strapi.plugins['email'].services.email.send({
        to: strapi.plugins['email'].services.email.config.defaultReplyTo,
        from: `${data.firstName} ${data.lastName} <${data.email}>`,
        subject: `Contact form request from website`,
        text: `
New Contact form request appeared on website:


${data.message}
        
`,
      })
    },
  },
}
