module.exports = {
  settings: {
    cors: {
      headers: ['X-Requested-With', 'Accept', 'Content-Type'],
      origin: [
        'http://localhost:1337',
        'http://localhost:5000',
        'http://localhost:3000',
        `https://london-luxury.bndigital.dev`,
        `https://cms.london-luxury.bndigital.dev`,
      ],
    },
    poweredBy: { enabled: false },
    responseTime: {
      enabled: false,
    },
  },
}
