const { basename, resolve } = require('path')

module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client:  process.env.NODE_ENV === 'development' ? 'sqlite':'pg',

        host: env('DATABASE_HOST', 'pg.bndigital.dev'),
        port: env.int('DATABASE_PORT', 5432),
        database: env('DATABASE_NAME', 'production'),
        schema: env('DATABASE_SCHEMA', 'london_luxury'),
        username: env('DATABASE_USERNAME', 'bn'),
        password: env('DATABASE_PASSWORD'),
      },
      options: {
        useNullAsDefault: true,
      },
    },
  },
})
