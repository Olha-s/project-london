module.exports = () => ({
  responses: {
    privateAttributes: ['created_by', 'updated_by', 'created_at', 'updated_at', 'published_at', '__v'],
  },
})
