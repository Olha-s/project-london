module.exports = ({ env }) => ({
  graphql: {
    endpoint: '/graphql',
    shadowCRUD: true,
    playgroundAlways: env('NODE_ENV', 'development') === 'development',
    depthLimit: 7,
    amountLimit: 250,
  },
  upload: {
    provider: 'aws-s3',
    providerOptions: {
      accessKeyId: env('AWS_ACCESS_KEY_ID'),
      secretAccessKey: env('AWS_ACCESS_SECRET'),
      region: env('AWS_REGION', 'us-east-1'),
      endpoint: env('AWS_S3_ENDPOINT', 'nyc3.digitaloceanspaces.com'),
      params: {
        Bucket: env('AWS_BUCKET', `bn-cdn/london-luxury/uploads`),
      },
    },
  },
  email: env('MAILGUN_API_KEY') ? {
    provider: 'mailgun',
    providerOptions: {
      apiKey: env('MAILGUN_API_KEY'),
      domain: env('MAILGUN_DOMAIN', 'mg.bndigital.dev'),
      host: env('MAILGUN_HOST', 'api.eu.mailgun.net'), //Optional. If domain region is Europe use 'api.eu.mailgun.net'
    },
    settings: {
      defaultFrom: env('MAILGUN_MAIL_FROM', 'dev@bndigital.co'),
      defaultReplyTo: env('MAILGUN_MAIL_TO', 'dev@bndigital.co'),
    },
  } : {},
})
